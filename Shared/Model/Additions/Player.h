//
//  Player+Additions.h
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//


#import <SpriteKit/SpriteKit.h>
#import "Node.h"

@interface Player : Node

@property (strong, nonatomic) NSNumber* dx;
@property (strong, nonatomic) NSNumber* dy;
@property (strong, nonatomic) NSNumber* currentLevel;
@property (nonatomic, strong) NSNumber* currentNumberOfLives;
@property (nonatomic, strong) NSNumber* maxNumberOfLives;
@property (strong, nonatomic) Node* currentNode;

+ (Player*)getPlayerForLevel:(Level*)level;

- (void)savePlayer;
- (void)playRev;

+ (NSArray*)pointsForCircleFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint usingOrigin:(CGPoint)origin;

+ (NSNumber*)getPlayersCurrentLevelFromPlist;

+ (void)setPlayersPlistToLevel:(NSNumber*)levelNumber;

@end