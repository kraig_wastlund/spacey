
//
//  GameScene.m
//  nodes1
//
//  Created by Kraig Wastlund on 11/9/15.
//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import "GameScene.h"
#import "MainScene.h"
#import "Node.h"
#import "Player.h"
#import "WormHole.h"
@import AVFoundation;
#import "SPCYSettings.h"
#import "SPCYAlertView.h"
#import "SPCYCropNode.h"
#import "SPCYLivesNode.h"
#import "User.h"
#import "spacey_iOS-Swift.h"
#import "SPCYConstants.h"
#import "UserLevel.h"

static const uint32_t playerCategory        =  0x1 << 0;
static const uint32_t middleCategory        =  0x1 << 1;
static const uint32_t wormHoleCategory      =  0x1 << 2;
static const uint32_t blackHoleCategory     =  0x1 << 3;
static const uint32_t endCategory           =  0x1 << 4;


@interface GameScene() <SKPhysicsContactDelegate, SPCYAlertDelegate>

@property (nonatomic, strong) Player* player;
@property (nonatomic) NSInteger totalPossibleLives;
@property (nonatomic) NSInteger currentLives;
@property (nonatomic, strong) SPCYLivesNode* livesNode;
@property (nonatomic) CGVector playerVector;
@property (nonatomic) CGFloat playerSpeedMultiplier;
@property (nonatomic) BOOL playerIsInsideNode;
@property (nonatomic) BOOL gameIsOver;

@property (nonatomic) BOOL passingThroughWormhole;

@property (nonatomic) CGPoint onNodePoint;

@property (nonatomic, strong) NSMutableArray* blackHolePoints;
@property (nonatomic) BOOL arcingAroundBlackHole;
@property (nonatomic, strong) NSMutableArray* arcPoints;
@property (nonatomic) CGPoint arcToPoint;

@property (nonatomic) CGPoint firstTouchPoint;

@property (nonatomic, strong) SKSpriteNode* backButton;

@property (nonatomic) CFTimeInterval startTime;
@property (nonatomic) CFTimeInterval timeForLevel;
@property (nonatomic, strong) SPCYCropNode* fuelGauge;
@property (nonatomic) NSInteger movesForLevel;
@property (nonatomic, strong) SPCYCropNode* oxygenGauge;

@property (nonatomic, strong) SKLabelNode* timerLabel;
@property (nonatomic, strong) SKLabelNode* movesLabel;
@property (nonatomic, strong) SKLabelNode* titleLabel;
@property (nonatomic, strong) SKLabelNode* inGameInfoLabel;

@property (nonatomic, strong) SKSpriteNode* fuelIcon;
@property (nonatomic, strong) SKSpriteNode* oxygenIcon;

@property (nonatomic) NSInteger gaugeWidth;

@property (nonatomic, strong) Level* gLevel;

@property (nonatomic, strong) User* user;

@property (nonatomic) float lastUpdateTime;
@property (nonatomic, strong) NSMutableArray* framesPerSecond;

@end


@implementation GameScene


#pragma mark - Contructor

- (instancetype)initWithSize:(CGSize)size
{
    // this gets called first
    // load sound, ai, unit stats that don't change
    self = [super initWithSize:size];
    if (self)
    {
        //FPS
        self.lastUpdateTime = 0.0;
        self.framesPerSecond = [[NSMutableArray alloc] init];
        
        self.physicsWorld.gravity = CGVectorMake(0, 0);
        self.playerIsInsideNode = YES;
        self.physicsWorld.contactDelegate = self;
        self.firstTouchPoint = CGPointMake(0, 0);
        [self setSize:size];
        self.gaugeWidth = self.frame.size.width * 0.20;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    return self;
}


#pragma mark - View Life Cycle

-(void)didMoveToView:(SKView*)view
{
    self.user = [User getUserInContext:self.managedObjectContext];
    self.user.fps = @([self getRecentAverageFPS]);
    
    if (self.levelNumber == nil)
    {
        self.levelNumber = self.user.lastPlayedLevel;
    }
    
    if (self.levelNumber.integerValue == 1)
    {
        SPCYAlertView* tutorialAlertView = [[SPCYAlertView alloc] initTutorialAlert];
        tutorialAlertView.alertDelegate = self;
        [self.view addSubview:tutorialAlertView];
        self.gameIsOver = YES;
    }
    else
    {
        [self startGame];
    }
}

- (void)willMoveFromView:(SKView*)view // gets called when the user proceeds to next level (not on dismiss)
{
    [self removeAllChildren];
    [self removeAllActions];
    [self removeFromParent];
}

- (void)startGame
{
    self.playerVector = CGVectorMake(0, 0);
    self.passingThroughWormhole = NO;
    self.blackHolePoints = [[NSMutableArray alloc] init];
    
    SKSpriteNode *backgroundNode = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    backgroundNode.size = self.frame.size;
    backgroundNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundNode.name = @"BACKGROUND";
    [self addChild:backgroundNode];
    [backgroundNode setZPosition:-10];
    
    self.gLevel = [Level getLevelWithName:self.levelNumber width:[NSNumber numberWithDouble:self.frame.size.width - 12] height:[NSNumber numberWithDouble:self.frame.size.width - 12]];
    self.startTime = 0;
    self.movesForLevel = self.gLevel.getNumberOfMoves;
    
    self.player = [Player getPlayerForLevel:self.gLevel];
    self.playerSpeedMultiplier = 1;
    self.totalPossibleLives = self.user.maxLives.integerValue;
    self.currentLives = self.user.lives.integerValue;
    
    // time
    self.timeForLevel = [self.gLevel getTimeForPlayerSpeed:self.player.speed andFPS:self.user.fps.doubleValue];
    
    // [self setupBackButton];
    [self setupLives];
    [self setupTitleLabel];
    [self setupInGameInfoLabel];
    [self setupOxygen];
    [self setupFuel];
    
    [self addChild:self.player];
    [self addChild:self.gLevel.start];
    [self addChild:self.gLevel.end];
    [self drawPlanetBehindNode:self.gLevel.end];
    
    for (Node* node in self.gLevel.middleNodes)
    {
        [self addChild:node];
    }
    
    for (Node* node in self.gLevel.blackHoles)
    {
        [self.blackHolePoints addObject:[NSValue valueWithCGPoint:node.position]];
        [self addChild:node];
    }
    
    for (Node* node in self.gLevel.wormHoles)
    {
        [self addChild:node];
    }
    
    self.arcingAroundBlackHole = NO;
    self.gameIsOver = NO;
    self.onNodePoint = self.gLevel.start.position;
}

- (void)resetScene
{
    [self removeAllActions];
    [self removeAllChildren];
    self.livesNode = nil;
    self.timerLabel = nil;
    self.oxygenGauge = nil;
    self.oxygenIcon = nil;
    self.fuelIcon = nil;
    self.titleLabel = nil;
    self.inGameInfoLabel = nil;
    self.movesLabel = nil;
    self.fuelGauge = nil;
    self.backButton = nil;
    self.player = nil;
    self.playerVector = CGVectorMake(0, 0);
    self.playerIsInsideNode = YES;
    [self.view presentScene:self];
}

- (void)endScene
{
    [self removeAllActions];
    [self removeAllChildren];
}


#pragma mark - Update

- (void)update:(CFTimeInterval)currentTime // gets called once per frame
{
    // frames
    float deltaTime = currentTime - self.lastUpdateTime;
    float currentFPS = 1 / deltaTime;
    [self.framesPerSecond addObject:@(currentFPS)];
    self.lastUpdateTime = currentTime;
    
    if (self.gameIsOver)
    {
        return;
    }
    
    // timer
    if (self.startTime == 0)
    {
        self.startTime = currentTime;
    }
    
    CGFloat timeLeft = self.timeForLevel - (currentTime - self.startTime);
    if (timeLeft <= 0)
    {
        [self.timerLabel setText:@"0.00"];
        [self.oxygenGauge fillNodeToPercent:1.0 withDuration:0];
        self.gameIsOver = YES;
        [self gameOverWithCode:@1];
        return;
    }
    
    [self.timerLabel setText:[NSString stringWithFormat:@"%.2f", timeLeft]];
    [self.oxygenGauge fillNodeToPercent:1 - (timeLeft / self.timeForLevel) withDuration:0];
    
    // if i'm not currently going around a black hole and I DO have speed (i'm moving)
    if (!self.arcingAroundBlackHole && (self.playerVector.dx != 0 || self.playerVector.dy != 0))
    {
        [self.player runAction:[SKAction moveBy:self.playerVector duration:0]];
    }
    else if (self.playerIsInsideNode) // i'm inside a node and not moving
    {
        return;
    }
    else if (self.arcingAroundBlackHole) // i'm currently going around a black hole
    {
        CGPoint thisPoint = [self.arcPoints[0] CGPointValue];
        [self.arcPoints removeObjectAtIndex:0];
        SKAction* move = [SKAction moveTo:thisPoint duration:0];
        [self.player runAction:move completion:^{
            if ([self.arcPoints count] == 0)
            {
                self.arcingAroundBlackHole = NO;
                self.arcPoints = nil;
                SKAction* move = [SKAction moveBy:self.playerVector duration:0]; // this is here to get passed the bh, if not you'd just keep going around
                [self.player runAction:move];
            }
        }];
    }
    
    
    // trail
    SKEmitterNode* en = [[SKEmitterNode alloc] init];
    [en setPosition:self.player.position];
    [en setParticleTexture:[SKTexture textureWithImageNamed:@"spaceyship"]];
    [en setParticleBirthRate:1]; // births per second
    [en setNumParticlesToEmit:1];
    [en setParticleSize:CGSizeMake(self.player.size.width * 0.85, self.player.size.height * 0.85)];
    [en setParticleLifetime:1.0]; // seconds of life
    [en runAction:[SKAction sequence:@[ [SKAction fadeOutWithDuration:0.08], [SKAction waitForDuration:1.0], [SKAction removeFromParent] ]]];
    [self addChild:en];
    
    if ([self playerHasLeftScreen])
    {
        self.gameIsOver = YES;
        [self gameOverWithCode:@1];
        return;
    }
    
    CGPoint possibleBlackHolePoint = [self adjacentBlackHolePoint];
    if (possibleBlackHolePoint.x != 0 && possibleBlackHolePoint.y != 0 && !self.arcingAroundBlackHole)
    {
        NSArray* points = [self controlPointsForStartingPoint:self.player.position blackHolePoint:possibleBlackHolePoint andDirection:self.playerVector]; // point1, point2, endingPoint
        self.playerVector = [self vectorForBHPoint:possibleBlackHolePoint]; // set the players direction, to be used after he's done going around the bh
        self.arcPoints = [[Player pointsForCircleFromPoint:self.player.position toPoint:[points[2] CGPointValue] usingOrigin:possibleBlackHolePoint] mutableCopy];
        self.arcingAroundBlackHole = YES;
    }
}


#pragma mark - FPS

- (float)getRecentAverageFPS
{
    int i = 0;
    float total = 0.0;
    
    for (NSNumber* fps in self.framesPerSecond.reverseObjectEnumerator)
    {
        ++i;
        total += fps.floatValue;
        if (i >= 10)
        {
            break;
        }
    }
    
    // reset array after grabbing recents (this is so the array doesn't get too big)
    self.framesPerSecond = [[NSMutableArray alloc] init];
    
    return total / i;
}


#pragma mark - Touches

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"back_button"])
    {
        [self gameOverWithCode:@0];
    }
    
    self.firstTouchPoint = [[touches anyObject] locationInView:self.view];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    if (self.movesLabel.text.integerValue <= 0) // i'm out of gas
    {
        [self triggerInGameInfoWithText:[SPCYSettings outOfGasMessage]];
        return;
    }
    
    if (self.firstTouchPoint.x != 0 && self.firstTouchPoint.y != 0)
    {
        if (self.playerVector.dx != 0 || self.playerVector.dy != 0) // if i'm currently moving, ignore touch
        {
            return;
        }
        
        UITouch* lastTouch = [touches anyObject];
        
        CGPoint location = [lastTouch locationInView:self.view];
        CGPoint prevLocation = self.firstTouchPoint;
        
        float dx = prevLocation.x - location.x;
        float dy = prevLocation.y - location.y;
        
        if (fabs(dx) > fabs(dy)) // swipe is mostly horizontal
        {
            if (fabs(dx) > 50) // the horizontal swipe is more than 10 points
            {
                if (dx > 0) // the horizontal swipe is right
                {
                    self.playerVector = CGVectorMake(-self.player.speed * self.playerSpeedMultiplier, 0.0);
                }
                else // the horizontal swipe is left
                {
                    self.playerVector = CGVectorMake(self.player.speed * self.playerSpeedMultiplier, 0.0);
                }
            }
        }
        else // swipe is mostly vertical
        {
            if (fabs(dy) > 50) // the vertical swipe is more than 10 points
            {
                if (dy > 0) // the vertical swipe is up
                {
                    self.playerVector = CGVectorMake(0.0, self.player.speed * self.playerSpeedMultiplier);
                }
                else // the vertical swipe is down
                {
                    self.playerVector = CGVectorMake(0.0, -self.player.speed * self.playerSpeedMultiplier);
                }
            }
        }
    }
    
    self.firstTouchPoint = CGPointMake(0, 0);
    if (self.playerVector.dx != 0 || self.playerVector.dy != 0)
    {
        //  self.playerIsInsideNode = NO; // do not set this here, this should be done when it actually leaves the node (endContact)
        CGFloat moves = self.movesLabel.text.intValue - 1;
        self.movesLabel.text = [NSString stringWithFormat:@"%.0f", moves];
        CGFloat percent = moves / self.movesForLevel;
        [self.fuelGauge fillNodeToPercent:1 - percent withDuration:0.35];
        [self playRev];
    }
}


#pragma mark - Collision

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if (self.playerIsInsideNode == YES || self.arcingAroundBlackHole || self.passingThroughWormhole)
    {
        if (self.passingThroughWormhole)
        {
            self.passingThroughWormhole = NO;
        }
        return;
    }
    
    self.playerIsInsideNode = YES;
    
    SKPhysicsBody* playerBody;
    SKPhysicsBody* secondBody;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask && contact.bodyA.categoryBitMask == playerCategory)
    {
        playerBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else if (contact.bodyB.categoryBitMask == playerCategory)
    {
        playerBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if (playerBody && secondBody)
    {
        SKAction* move;
        BOOL didWin = NO;
        BOOL didDie = NO;
        self.onNodePoint = secondBody.node.position;
        
        if (secondBody.categoryBitMask == wormHoleCategory)
        {
            WormHole* wormhole = (WormHole*)secondBody.node;
            [self.player runAction:[SKAction moveTo:wormhole.partnerNode.position duration:0.0]];
            self.passingThroughWormhole = YES;
            self.playerIsInsideNode = NO;
        }
        else if (secondBody.categoryBitMask == endCategory)
        {
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
            self.gameIsOver = YES;
            didWin = YES;
        }
        else if (secondBody.categoryBitMask == blackHoleCategory)
        {
            self.gameIsOver = YES;
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
            
            didDie = YES;
        }
        else if (secondBody.categoryBitMask == middleCategory) // middle
        {
            move = [SKAction moveTo:secondBody.node.position duration:0.0];
        }
        
        if (move != nil)
        {
            [self.player runAction:move];
            self.playerVector = CGVectorMake(0, 0);
        }
        
        if (didWin) // landed on end
        {
            [self performSelector:@selector(didWin) withObject:nil afterDelay:0.25];
        }
        else if (didDie) // landed on black hole
        {
            [self.player removeActionForKey:@"hover"];
            [self playVibrate];
            SKAction* rotateAction = [SKAction rotateByAngle:M_PI*4 duration:10];
            SKAction* scaleAction = [SKAction scaleTo:0.0 duration:10];
            [self.player runAction:rotateAction];
            [self.player runAction:scaleAction completion:^{
                [self gameOverWithCode:@1];
            }];
        }
    }
}

- (void)didEndContact:(SKPhysicsContact*)contact
{
    if (!self.passingThroughWormhole && (self.playerVector.dx != 0 || self.playerVector.dy != 0))
    {
        self.playerIsInsideNode = NO;
    }
}


#pragma mark - Sounds

- (void)playVibrate
{
    if (self.user.vibrateOn)
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

- (void)playDeath
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringDeath waitForCompletion:NO]];
    }
}

- (void)playWon
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringWin waitForCompletion:NO]];
    }
}

- (void)playRev
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringRev waitForCompletion:NO]];
    }
}

- (void)playIdle
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringIdle waitForCompletion:NO]];
    }
}


#pragma mark - UI Elements

- (void)setupBackButton
{
    if (!self.backButton)
    {
        self.backButton = [[SKSpriteNode alloc] initWithImageNamed:@"stop"];
        [self.backButton setPosition:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height * 0.91)];
        [self.backButton setSize:CGSizeMake(self.view.frame.size.width * 0.13, self.view.frame.size.width * 0.13)];
        [self.backButton setName:@"back_button"];
        [self.backButton setZPosition:2];
        [self addChild:self.backButton];
    }
}

- (void)setupLives
{
    if (!self.livesNode)
    {
        self.livesNode = [SPCYLivesNode sharedLivesNodeInContext:self.managedObjectContext];
        [self.livesNode setPosition:CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 30)];
        [self addChild:self.livesNode];
    }
}

- (void)setupTitleLabel
{
    if (!self.titleLabel)
    {
        self.titleLabel = [[SKLabelNode alloc] initWithFontNamed:@"HelveticaNeue-Light"];
        [self.titleLabel setFontColor:[SPCYSettings spcyPinkColor]];
        [self.titleLabel setPosition:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height * 0.80)];
        [self.titleLabel setFontSize:30];
        [self.titleLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
        [self.titleLabel setText:[NSString stringWithFormat:@"Planet %03ld", (long)self.levelNumber.integerValue]];
        [self addChild:self.titleLabel];
    }
}

- (void)setupInGameInfoLabel
{
    if (!self.inGameInfoLabel)
    {
        self.inGameInfoLabel = [[SKLabelNode alloc] initWithFontNamed:@"HelveticaNeue"];
        [self.inGameInfoLabel setFontColor:[SPCYSettings spcyPinkColor]];
        [self.inGameInfoLabel setPosition:CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2)];
        [self.inGameInfoLabel setFontSize:20];
        [self.inGameInfoLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
        self.inGameInfoLabel.text = @"";
        [self addChild:self.inGameInfoLabel];
        self.inGameInfoLabel.zPosition = 49;
    }
}

- (void)triggerInGameInfoWithText:(NSString*)text
{
    self.inGameInfoLabel.text = text;
    CGSize backgroundSize = CGSizeMake(self.inGameInfoLabel.frame.size.width + 10, self.inGameInfoLabel.frame.size.height + 10);
    SKSpriteNode* labelBackground = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:backgroundSize];
    [self.inGameInfoLabel addChild:labelBackground];
    [labelBackground setPosition:CGPointMake(0, self.inGameInfoLabel.frame.size.height / 2)];
    
    [self.inGameInfoLabel runAction:[SKAction fadeInWithDuration:0.5] completion:^{
        [self.inGameInfoLabel runAction:[SKAction waitForDuration:1.0] completion:^{
            [self.inGameInfoLabel runAction:[SKAction fadeOutWithDuration:0.5]];
            [self.inGameInfoLabel removeAllChildren];
        }];
    }];
}

- (void)setupOxygen
{
    if (!self.timerLabel)
    {
        self.timerLabel = [[SKLabelNode alloc] initWithFontNamed:@"HelveticaNeue-UltraLight"];
        [self.timerLabel setFontColor:[UIColor whiteColor]];
        [self.timerLabel setPosition:CGPointMake(15, self.frame.size.height - 60)];
        [self.timerLabel setFontSize:20];
        [self.timerLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
        [self addChild:self.timerLabel];
        [self.timerLabel setText:[NSString stringWithFormat:@"%.2f", self.timeForLevel]];
    }
    
    if (!self.oxygenGauge)
    {
        self.oxygenGauge = [[SPCYCropNode alloc] initCropNodeWithFrameImageName:@"oxygenbar" maskImageName:@"defaultMask" fillObject:[[SPCYSettings colorFromHexString:@"15151d"] colorWithAlphaComponent:0.8] size:CGSizeMake(self.gaugeWidth, 10) initialFillPlacement:kPlacementRight];
        [self.oxygenGauge setPosition:CGPointMake(self.gaugeWidth / 2 + 15, self.frame.size.height - 30)];
        [self.oxygenGauge setName:@"oxygen"];
        [self addChild:self.oxygenGauge];
    }
    
    if (!self.oxygenIcon)
    {
        int height = 25;
        self.oxygenIcon = [[SKSpriteNode alloc] initWithImageNamed:@"oxygenIcon"];
        CGFloat ratio = self.oxygenIcon.size.width / self.oxygenIcon.size.height;
        [self.oxygenIcon setSize:CGSizeMake(height * ratio, height)];
        [self.oxygenIcon setPosition:CGPointMake(self.oxygenGauge.position.x + self.oxygenGauge.size.width * .60, self.oxygenGauge.position.y)];
        [self addChild:self.oxygenIcon];
    }
}

- (void)setupFuel
{
    if (!self.movesLabel)
    {
        self.movesLabel = [[SKLabelNode alloc] initWithFontNamed:@"HelveticaNeue-UltraLight"];
        [self.movesLabel setFontColor:[UIColor whiteColor]];
        [self.movesLabel setPosition:CGPointMake(self.view.frame.size.width - 15, self.frame.size.height - 60)];
        [self.movesLabel setFontSize:20];
        [self.movesLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeRight];
        [self addChild:self.movesLabel];
        [self.movesLabel setText:[NSString stringWithFormat:@"%ld", (long)self.movesForLevel]];
    }
    
    if (!self.fuelGauge)
    {
        self.fuelGauge = [[SPCYCropNode alloc] initCropNodeWithFrameImageName:@"fuelbar" maskImageName:@"defaultMask" fillObject:[[SPCYSettings colorFromHexString:@"15151d"] colorWithAlphaComponent:0.7] size:CGSizeMake(self.gaugeWidth, 10) initialFillPlacement:kPlacementLeft];
        [self.fuelGauge setPosition:CGPointMake(self.view.frame.size.width - (self.gaugeWidth / 2 + 15), self.frame.size.height - 30)];
        [self.fuelGauge setName:@"fuel"];
        [self addChild:self.fuelGauge];
    }
    
    if (!self.fuelIcon)
    {
        int height = 25;
        self.fuelIcon = [[SKSpriteNode alloc] initWithImageNamed:@"fuelIcon"];
        CGFloat ratio = self.fuelIcon.size.width / self.fuelIcon.size.height;
        [self.fuelIcon setSize:CGSizeMake(height * ratio, height)];
        [self.fuelIcon setPosition:CGPointMake(self.self.fuelGauge.position.x - self.self.fuelGauge.size.width * .60, self.fuelGauge.position.y)];
        [self addChild:self.fuelIcon];
    }
}

- (void)drawPlanetBehindNode:(Node*)node
{
    CGSize circleSize = CGSizeMake(node.size.width * 0.9, node.size.height * 0.9);
    CGRect circle = CGRectMake(node.position.x - circleSize.width / 2, node.position.y - circleSize.height / 2, circleSize.width, circleSize.height);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.fillColor = [[SPCYSettings planetColors] objectAtIndex:self.levelNumber.integerValue % [[SPCYSettings planetColors] count]];
    shapeNode.lineWidth = 0;
    shapeNode.name = @"planet_background";
    [self addChild:shapeNode];
}

- (void)removePlanetBackgroundsFromView
{
    for (SKShapeNode* node in self.children)
    {
        if ([node.name isEqualToString:@"planet_background"])
        {
            [node removeFromParent];
        }
    }
}

- (CGVector)vectorForBHPoint:(CGPoint)bhPoint
{
    CGPoint sPoint = self.player.position;
    CGVector vector = self.playerVector;
    
    CGVector retVector;
    if (vector.dx != 0) // moving horizontally
    {
        if (bhPoint.y > sPoint.y) // player is below the blackhole
        {
            retVector = CGVectorMake(0, self.player.speed * self.playerSpeedMultiplier);
        }
        else // player is above the blackhole
        {
            retVector = CGVectorMake(0, -self.player.speed * self.playerSpeedMultiplier);
        }
    }
    else // moving vertically
    {
        if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
        {
            retVector = CGVectorMake(self.player.speed * self.playerSpeedMultiplier, 0);
        }
        else // player is to the right of the blackhole
        {
            retVector = CGVectorMake(-self.player.speed * self.playerSpeedMultiplier, 0);
        }
    }
    
    return retVector;
}


#pragma mark - Ship Tracking

- (CGPoint)adjacentBlackHolePoint
{
    // first check the players distance from all blackholes
    // if i'm close to one then populate the bhPoint
    CGFloat minRadius = self.gLevel.cellSize.width * 1.05; // might need to factor pixel density here. TODO: Kraig
    CGPoint bhPoint = CGPointMake(0, 0);
    
    if (self.playerIsInsideNode)
    {
        return bhPoint;
    }
    
    CGPoint playerPos = self.player.position;
    CGFloat distance;
    for (id pos in self.blackHolePoints)
    {
        CGPoint thisBHPoint = [pos CGPointValue];
        CGFloat xDist = (playerPos.x - thisBHPoint.x);
        CGFloat yDist = (playerPos.y - thisBHPoint.y);
        distance = sqrt((xDist * xDist) + (yDist * yDist));
        if (distance <= minRadius)
        {
            bhPoint = thisBHPoint;
            break;
        }
    }
    
    // now make sure the player is'nt headed right for that blackhole
    // and if i'm along side the bh, check to see if i've already passed it
    float maxMargin = 5.0;
    if (bhPoint.x != 0 || bhPoint.y != 0)
    {
        if (self.playerVector.dy > 0) // moving upwards
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.x - bhPoint.x) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.y < self.player.position.y) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dy < 0) // moving downwards
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.x - bhPoint.x) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.y > self.player.position.y) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dx < 0) // moving left
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.y - bhPoint.y) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.x > self.player.position.x) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
        else if (self.playerVector.dx > 0) // moving right
        {
            // first check if the player is headed right for the bh
            if (fabs(self.player.position.y - bhPoint.y) < maxMargin)
            {
                return CGPointMake(0, 0);
            }
            
            if (bhPoint.x < self.player.position.x) // i've already past the blackhole
            {
                bhPoint = CGPointMake(0, 0);
            }
        }
    }
    
    return bhPoint;
}

- (BOOL)playerHasLeftScreen
{
    if (self.player.position.x + self.player.size.width < 0)
    {
        return YES;
    }
    if (self.player.position.y + self.player.size.height < 0)
    {
        return YES;
    }
    if (self.player.position.x - self.player.size.width > self.view.frame.size.width)
    {
        return YES;
    }
    if (self.player.position.y - self.player.size.height > self.view.frame.size.height)
    {
        return YES;
    }
    
    return NO;
}


#pragma mark - Special Black Hole Methods

- (SKAction*)curveActionWithStartingPoint:(CGPoint)startingPoint blackHolePoint:(CGPoint)blackHolePoint
{
    CGMutablePathRef cgpath = CGPathCreateMutable();
    
    NSArray* curvePoints = [self controlPointsForStartingPoint:startingPoint blackHolePoint:blackHolePoint andDirection:self.playerVector];
    
    CGPoint controlPoint1 = [curvePoints[0] CGPointValue];
    CGPoint controlPoint2 = [curvePoints[1] CGPointValue];
    CGPoint endingPoint = [curvePoints[2] CGPointValue];
    
    CGPathMoveToPoint(cgpath, NULL, startingPoint.x, startingPoint.y);
    CGPathAddCurveToPoint(cgpath, NULL, controlPoint1.x, controlPoint1.y, controlPoint2.x, controlPoint2.y, endingPoint.x, endingPoint.y);
    
    SKAction* playerAroundBHCurve = [SKAction followPath:cgpath asOffset:NO orientToPath:YES duration:5];
    
    return [SKAction sequence:@[[SKAction waitForDuration:1],playerAroundBHCurve]];
}

- (NSArray*)controlPointsForStartingPoint:(CGPoint)sPoint blackHolePoint:(CGPoint)bhPoint andDirection:(CGVector)vector
{
    CGPoint endingPoint;
    CGPoint point1;
    CGPoint point2;
    
    CGFloat xDelta = self.gLevel.cellSize.width * 0.25;
    CGFloat yDelta = self.gLevel.cellSize.height * 0.25;
    
    if (vector.dx != 0) // moving horizontally
    {
        if (vector.dx < 0) // moving left
        {
            endingPoint.x = bhPoint.x - self.gLevel.cellSize.width;
            endingPoint.y = bhPoint.y;
            
            if (bhPoint.y > sPoint.y) // player is below the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y - yDelta);
            }
            else // player is above the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y + yDelta);
            }
        }
        else // moving right
        {
            endingPoint.x = bhPoint.x + self.gLevel.cellSize.width;
            endingPoint.y = bhPoint.y;
            
            if (bhPoint.y > sPoint.y) // player is below the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y - yDelta);
            }
            else // player is above the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y + yDelta);
            }
        }
    }
    else // moving vertically
    {
        if (vector.dy < 0) // moving down
        {
            endingPoint.x = bhPoint.x;
            endingPoint.y = bhPoint.y - self.gLevel.cellSize.height;
            
            if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y - yDelta);
            }
            else // player is to the right of the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y - yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y - yDelta);
            }
        }
        else // moving up
        {
            endingPoint.x = bhPoint.x;
            endingPoint.y = bhPoint.y + self.gLevel.cellSize.height;
            
            if (bhPoint.x > sPoint.x) // player is to the left of the blackhole
            {
                point1 = CGPointMake(sPoint.x - xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x - xDelta, bhPoint.y + yDelta);
            }
            else // player is to the right of the blackhole
            {
                point1 = CGPointMake(sPoint.x + xDelta, sPoint.y + yDelta);
                point2 = CGPointMake(bhPoint.x + xDelta, bhPoint.y + yDelta);
            }
        }
    }
    
    return @[[NSValue valueWithCGPoint:point1], [NSValue valueWithCGPoint:point2], [NSValue valueWithCGPoint:endingPoint]];
}


#pragma mark - Game Over

- (void)gameOverWithCode:(NSNumber*)codeNum // 0 = quit, 1 = lost, 2 = win
{
    [[self childNodeWithName:@"the_finger"] removeFromParent]; // remove the finger if it's on the scene
    
    FinishedGameCode gameCode;
    if (codeNum.integerValue == 0)
    {
        gameCode = kGameQuit;
        self.currentLives -= 1;
        self.user.lives = [NSNumber numberWithInteger:self.currentLives];
        [self playDeath];
    }
    else if (codeNum.integerValue == 1)
    {
        gameCode = kGameLost;
        self.currentLives -= 1;
        self.user.lives = [NSNumber numberWithInteger:self.currentLives];
        [self playDeath];
    }
    else
    {
        UserLevel* currentLevel = [UserLevel getUserLevelWithLevelNumber:self.levelNumber InContext:self.managedObjectContext];
        [currentLevel setBestTime:@(self.timeForLevel - self.timerLabel.text.doubleValue)];
        [currentLevel setBestMoves:@(self.movesForLevel - self.movesLabel.text.integerValue)];
        self.user.highestCompletedLevel = self.levelNumber;
        gameCode = kGameWon;
        [self playWon];
    }
    
    [self.livesNode reactToLivesChangedInContext:self.managedObjectContext];
    [self performSelector:@selector(removeChildrenWithGameCode:) withObject:[NSNumber numberWithInt:gameCode] afterDelay:1.5];
}

- (void)removeChildrenWithGameCode:(NSNumber*)gameCodeNumber
{
    FinishedGameCode gameCode = [gameCodeNumber intValue];
    [self removeChildrenInArray:self.gLevel.nodes];
    [self.inGameInfoLabel removeFromParent];
    [self.backButton removeFromParent];
    [self.livesNode removeFromParent];
    [self.fuelIcon removeFromParent];
    [self.oxygenIcon removeFromParent];
    [self.player removeFromParent];
    [self.timerLabel removeFromParent];
    [self.oxygenGauge removeFromParent];
    [self.movesLabel removeFromParent];
    [self.fuelGauge removeFromParent];
    [self removePlanetBackgroundsFromView];
    [self setPaused:YES];
    CGRect alertFrame = CGRectMake(0, 0, self.size.width, self.titleLabel.position.y - self.titleLabel.frame.size.height / 2);
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithFrame:alertFrame levelNumber:self.levelNumber currentTime:@(self.timeForLevel - self.timerLabel.text.doubleValue) currentMoves:[NSNumber numberWithInteger:self.movesForLevel - self.movesLabel.text.integerValue] numberOfLives:@(self.currentLives) fps:self.user.fps playerSpeed:[NSNumber numberWithFloat:self.player.speed] level:self.gLevel andGameCode:gameCode plusContext:self.managedObjectContext];
    alertView.alertDelegate = self;
    alertView.alpha = 0.0;
    [self.view addSubview:alertView];
    [UIView animateWithDuration:0.5
                     animations:^{alertView.alpha = 1.0;}
                     completion:nil];
}

- (void)didWin
{
    [self.player runAction:[SKAction rotateToAngle:0 duration:1.0] completion:^{
        [self.player removeActionForKey:@"hover"]; // remove hover
    }];
    
    SKAction* scaleAction = [SKAction scaleTo:0.0 duration:10];
    [self.player runAction:scaleAction completion:^{
        NSNumber* nextLevel = [NSNumber numberWithInteger:self.levelNumber.integerValue + 1];
        if (self.player.currentLevel.integerValue < nextLevel.integerValue)
        {
            if (nextLevel.integerValue < [Level getTotalNumberOfLevels])
            {
                self.player.currentLevel = nextLevel;
                self.user.lastPlayedLevel = self.player.currentLevel;
            }
        }
        [self.player savePlayer];
        [self gameOverWithCode:@2];
    }];
}


#pragma mark - Sounds

- (void)playClick
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringClick waitForCompletion:NO]];
    }
}


#pragma mark - AlertViewDelegate

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self playClick];
    });
    
    if (buttonType == kTypeLeft) // replay
    {
        if (self.user.lives.integerValue > 0)
        {
            [self resetScene];
            [self removeAlertView:view];
        }
    }
    else if (buttonType == kTypeMiddle) // play next
    {
        self.levelNumber = [NSNumber numberWithInteger:self.levelNumber.integerValue + 1];
        [self resetScene];
        [self removeAlertView:view];
    }
    else if (buttonType == kTypeRight) // menu
    {
        MainScene* game = (MainScene*)[(GameViewController*)[[self.view window] rootViewController] mainMenu]; // grab the main menu from the GameViewController
//        game.scaleMode = SKSceneScaleModeResizeFill;
//        game.managedObjectContext = self.managedObjectContext;
        [self.view presentScene:game transition:[SKTransition crossFadeWithDuration:1.0]];
        [self removeAlertView:view];
        [self endScene];
        [game updateLives];
    }
    else if (buttonType == kTypeTutorial)
    {
        // show the finger
        SKSpriteNode* theFinger = [SKSpriteNode spriteNodeWithImageNamed:@"the_finger"];
        [theFinger setPosition:CGPointMake(self.player.position.x, self.player.position.y - 30)];
        [theFinger setAnchorPoint:CGPointMake(0.5, 1)];
        [theFinger runAction:[SKAction rotateByAngle:1 * M_PI / 12 duration:0.0]];
        [theFinger setSize:CGSizeMake(self.player.size.width * 1.25, self.player.size.width * 4)];
        [theFinger setAlpha:0.0];
        [theFinger setName:@"the_finger"];
        [self addChild:theFinger];
        
        SKAction* animateFinger = [SKAction sequence:@[
                                                       [SKAction waitForDuration:1.0],
                                                       [SKAction fadeInWithDuration:0.5],
                                                       [SKAction group:@[ [SKAction scaleBy:0.8 duration:0.5], [SKAction moveBy:CGVectorMake(-4, -10) duration:0.5] ]],
                                                       [SKAction moveBy:CGVectorMake(self.view.frame.size.width + 100, 0) duration:2.0],
                                                       ]];
        [theFinger runAction:animateFinger completion:^{[theFinger removeFromParent];}];
        self.gameIsOver = NO;
        [self startGame];
    }
}

- (void)removeAlertView:(SPCYAlertView*)view
{
    [UIView animateWithDuration:0.5
                     animations:^{view.alpha = 0.0;}
                     completion:^(BOOL finished){ [view removeFromSuperview]; }];
}

@end
