//
//  MoonNode.h
//  spacey
//
//  Created by Chad Olsen on 11/22/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class MenuPlanet;

@interface MoonNode : SKShapeNode

@property (nonatomic, strong) MenuPlanet* parentPlanet;

- (void)moveMoon;

@end
