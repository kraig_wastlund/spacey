//
//  BlackHole.m
//  spacey
//
//  Created by Donald Timpson on 11/29/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "BlackHole.h"
#import "Level.h"

@implementation BlackHole

+ (BlackHole*)setupBlackHoleWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level
{
    BlackHole* blackHole = [[BlackHole alloc] init];
    blackHole.type = [nodeDict objectForKey:@"type"];
    blackHole.x = [nodeDict objectForKey:@"x"];
    blackHole.y = [nodeDict objectForKey:@"y"];
    blackHole.name = @"blackhole";
    
    [blackHole setTexture:[SKTexture textureWithImageNamed:@"blackhole"]];
    SKAction *oneRevolution = [SKAction rotateByAngle:-M_PI*2 duration: 5.0];
    SKAction *repeat = [SKAction repeatActionForever:oneRevolution];
    [blackHole runAction:repeat];
    
    blackHole.size = CGSizeMake(level.cellSize.width * 1.8, level.cellSize.height * 1.8);
    CGFloat deltaY = [[UIScreen mainScreen] bounds].size.height * 0.16;
    CGFloat deltaX = 6;
    blackHole.position = CGPointMake(.5 * level.cellSize.width + blackHole.x.doubleValue * level.cellSize.width + deltaX, .5 * level.cellSize.height + blackHole.y.doubleValue * level.cellSize.height + deltaY);
    [blackHole setZPosition:-1];
    
    return blackHole;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
