//
//  User.m
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "User.h"
#import "KoldProducts.h"
#import "SPCYLivesNode.h"
#import "SPCYSettings.h"
#import "NSDate+NetworkClock.h"

static const NSInteger lifeRegenerationTime = 20 * 60;

@implementation User

+ (User*)setUpDefaultUserInContext:(NSManagedObjectContext*)context // gets called only on first install of app
{
    
    NSNumber* regenx2 = [[NSUserDefaults standardUserDefaults] objectForKey:spaceyIAP2xRegen()];
    NSNumber* regenx3 = [[NSUserDefaults standardUserDefaults] objectForKey:spaceyIAP3xRegen()];
    NSNumber* lives5 = [[NSUserDefaults standardUserDefaults] objectForKey:spaceyIAP5Buckets()];
    NSNumber* lives7 = [[NSUserDefaults standardUserDefaults] objectForKey:spaceyIAP7Buckets()];
    
    User* user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    
    user.livesModifier = [NSNumber numberWithDouble:1.0];
    if (regenx2.boolValue)
    {
        user.livesModifier = [NSNumber numberWithDouble:0.0 / 2.0];
    }
    if (regenx3.boolValue)
    {
        user.livesModifier = [NSNumber numberWithDouble:0.0 / 3.0];
    }
    
    user.maxLives = @3;
    user.lives = @3; // This has to be set AFTER max lives
    if (lives5.boolValue)
    {
        user.maxLives = @5;
        user.lives = @5; // This has to be set AFTER max lives
    }
    if (lives7.boolValue)
    {
        user.maxLives = @7;
        user.lives = @7; // This has to be set AFTER max lives
    }
    
    user.fuelModifier = @1;
    user.highestCompletedLevel = @0;
    user.lastPlayedLevel = @1;
    user.musicOn = @NO;
    user.oxygenModifier = @1;
    user.soundEffectsOn = @NO;
    user.vibrateOn = @YES;
    user.livesUpdatedOn = [NSDate networkDate];
    user.fps = [NSNumber numberWithDouble:50.0];
    
    NSError *error = nil;
    if (![context save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return user;
}

+ (User*)getUserInContext:(NSManagedObjectContext*)context
{
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    User* user = [[context executeFetchRequest:request error:nil] firstObject];
    
    if (user == nil)
    {
        user = [User setUpDefaultUserInContext:context];
    }
    
    return user;
}

- (void)setFuelModifier:(NSNumber *)fuelModifier
{
    [self willChangeValueForKey:@"fuelModifier"];
    [self setPrimitiveValue:fuelModifier forKey:@"fuelModifier"];
    [self didChangeValueForKey:@"fuelModifier"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setHighestCompletedLevel:(NSNumber *)highestCompletedLevel
{
    [self willChangeValueForKey:@"highestCompletedLevel"];
    [self setPrimitiveValue:highestCompletedLevel forKey:@"highestCompletedLevel"];
    [self didChangeValueForKey:@"highestCompletedLevel"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setLastPlayedLevel:(NSNumber *)lastPlayedLevel
{
    [self willChangeValueForKey:@"lastPlayedLevel"];
    [self setPrimitiveValue:lastPlayedLevel forKey:@"lastPlayedLevel"];
    [self didChangeValueForKey:@"lastPlayedLevel"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setLives:(NSNumber *)lives
{
    if (lives.integerValue > self.maxLives.integerValue)
    {
        lives = self.maxLives;
    }
    
    [self willChangeValueForKey:@"lives"];
    [self setPrimitiveValue:lives forKey:@"lives"];
    [self didChangeValueForKey:@"lives"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setLivesModifier:(NSNumber *)livesModifier
{
    [self willChangeValueForKey:@"livesModifier"];
    [self setPrimitiveValue:livesModifier forKey:@"livesModifier"];
    [self didChangeValueForKey:@"livesModifier"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setMaxLives:(NSNumber *)maxLives
{
    [self willChangeValueForKey:@"maxLives"];
    [self setPrimitiveValue:maxLives forKey:@"maxLives"];
    [self didChangeValueForKey:@"maxLives"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
    
    if (self.lives < self.maxLives) {
        self.lives = self.maxLives;
    }
}

- (void)setMusicOn:(NSNumber *)musicOn
{
    [self willChangeValueForKey:@"musicOn"];
    [self setPrimitiveValue:musicOn forKey:@"musicOn"];
    [self didChangeValueForKey:@"musicOn"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setOxygenModifier:(NSNumber *)oxygenModifier
{
    [self willChangeValueForKey:@"oxygenModifier"];
    [self setPrimitiveValue:oxygenModifier forKey:@"oxygenModifier"];
    [self didChangeValueForKey:@"oxygenModifier"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setSoundEffectsOn:(NSNumber *)soundEffectsOn
{
    [self willChangeValueForKey:@"soundEffectsOn"];
    [self setPrimitiveValue:soundEffectsOn forKey:@"soundEffectsOn"];
    [self didChangeValueForKey:@"soundEffectsOn"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setVibrateOn:(NSNumber *)vibrateOn
{
    [self willChangeValueForKey:@"vibrateOn"];
    [self setPrimitiveValue:vibrateOn forKey:@"vibrateOn"];
    [self didChangeValueForKey:@"vibrateOn"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setFps:(NSNumber *)fps
{
    if (fps.doubleValue > 0)
    {
        [self willChangeValueForKey:@"fps"];
        [self setPrimitiveValue:fps forKey:@"fps"];
        [self didChangeValueForKey:@"fps"];
    
        NSError* error;
        [self.managedObjectContext save:&error];
        assert(error == nil);
    }
}

- (NSNumber*)lives
{
    NSInteger livesToAdd = -1;
    NSDate* updatedToDate = self.livesUpdatedOn;
    
    while (updatedToDate.timeIntervalSince1970 < [[NSDate networkDate] timeIntervalSince1970])
    {
        self.livesUpdatedOn = updatedToDate;
        updatedToDate = [NSDate dateWithTimeInterval:(lifeRegenerationTime * self.livesModifier.doubleValue) sinceDate:updatedToDate];
        livesToAdd += 1;
    }
    
    [self willAccessValueForKey:@"lives"];
    NSNumber *lives = [self primitiveValueForKey:@"lives"];
    [self didAccessValueForKey:@"lives"];
    
    self.lives = [NSNumber numberWithInteger:(lives.integerValue + livesToAdd)];
    
    [self willAccessValueForKey:@"lives"];
    lives = [self primitiveValueForKey:@"lives"];
    [self didAccessValueForKey:@"lives"];
    
    return lives;
}

- (NSString*)timeTilNextRegenerationMessage
{
    NSString* retString;
    
    NSTimeInterval distance = [[self livesUpdatedOn] timeIntervalSinceDate:[NSDate networkDate]];
    NSTimeInterval timeUntil = (lifeRegenerationTime / self.livesModifier.doubleValue) - fabs(distance);
    int timeUntilInMinutes = timeUntil / 60;
    if (timeUntilInMinutes == 0)
    {
        timeUntilInMinutes = 1;
    }
    retString = [NSString stringWithFormat:@"Next life in: %d min", timeUntilInMinutes];
    
    return retString;
}

- (NSNumber*)livesModifier
{
    [self willAccessValueForKey:@"livesModifier"];
    NSNumber *livesModifier = [self primitiveValueForKey:@"livesModifier"];
    [self didAccessValueForKey:@"livesModifier"];
    
    if ([SPCYSettings spcyDebugToggle])
    {
        return [NSNumber numberWithDouble:0.1]; // default is 20 min (setting the modifier will reduce or increase the life regeneration accordingly)
    }
    else
    {
        return livesModifier;
    }
}

@end
