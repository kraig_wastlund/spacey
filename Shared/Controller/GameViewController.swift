//
//  GameViewController.swift
//  spacey
//
//  Created by Kaden Wilkinson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

import AVFoundation
import SpriteKit
import UIKit
import CoreData

@objc class GameViewController : UIViewController, SPCYAlertDelegate {

    var managedObjectContext: NSManagedObjectContext?
    var levelNumber: NSNumber?
    var skView: SKView!
    var lifeTimer: NSTimer!
    var mainMenu: MainScene?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        lifeTimer.invalidate()
    }
    
    override func awakeFromNib() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skView = self.view as! SKView
        skView.ignoresSiblingOrder = true
        skView.backgroundColor = UIColor.clearColor()
        
        if SPCYSettings.spcyDebugToggle() == true {
            debuggingMode()
        }
        
        mainMenu = MainScene(size: view.frame.size)
        mainMenu!.managedObjectContext = managedObjectContext
        skView.presentScene(mainMenu)
    }
    
    func debuggingMode() {
        self.skView.showsFPS = true
        self.skView.showsDrawCount = true
        self.skView.showsNodeCount = true
        self.skView.showsQuadCount = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        handleMusic()
        refreshLivesNode()
        lifeTimer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: #selector(getLives), userInfo: nil, repeats: true)
    }
    
    func refreshLivesNode() {
        mainMenu!.updateLives()
    }
    
    func handleMusic() {
        let currentUser = User.getUserInContext(managedObjectContext!)
        if currentUser.musicOn == true {
            MusicHelper.sharedHelper.playBackgroundMusic()
        } else {
            MusicHelper.sharedHelper.stopPlaying()
        }
    }
    
    func getLives() {
        let currentUser = User.getUserInContext(managedObjectContext!)
        let _ = currentUser.lives
        let userLivesNode = SPCYLivesNode.sharedLivesNodeInContext(managedObjectContext!)
        userLivesNode.reactToLivesChangedInContext(managedObjectContext!)
        mainMenu!.reactToTimer()
    }
}

