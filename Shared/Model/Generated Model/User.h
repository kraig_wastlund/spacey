//
//  User.h
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (User*)getUserInContext:(NSManagedObjectContext*)context;
- (NSString*)timeTilNextRegenerationMessage;

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
