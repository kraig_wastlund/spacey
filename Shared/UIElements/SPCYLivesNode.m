//
//  SPCYLivesNode.m
//  spacey
//
//  Created by Kraig Wastlund on 5/13/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "SPCYLivesNode.h"
#import "SPCYSettings.h"
#import "User.h"

@implementation SPCYLivesNode

+ (SPCYLivesNode*)sharedLivesNodeInContext:(NSManagedObjectContext*)context
{
    static SPCYLivesNode* sharedInstance = nil;
    
    User* currentUser = [User getUserInContext:context];
    if (sharedInstance.children.count - 1 != currentUser.maxLives.integerValue)
    {
        User* currentUser = [User getUserInContext:context];
        sharedInstance = [[SPCYLivesNode alloc] initCropNodeWithFrameImageName:@"defaultFrame" maskImageName:@"defaultMask" fillObject:[[SPCYSettings colorFromHexString:@"15151d"] colorWithAlphaComponent:0.6] size:CGSizeMake([SPCYSettings lifeNodeWidth] * currentUser.maxLives.integerValue, [SPCYSettings lifeNodeWidth]) initialFillPlacement:kPlacementRight];// 15151d
        sharedInstance.name = @"lives";
        for (int i = 1; i <= currentUser.maxLives.integerValue; i ++)
        {
            int mult = i / 2;
            int neg = i % 2 == 0 ? -1 : 1;
            SKSpriteNode* lifeNode = [[SKSpriteNode alloc] initWithImageNamed:@"life"];
            CGFloat ratio = lifeNode.size.width / lifeNode.size.height;
            float lifeDimm = [SPCYSettings lifeNodeWidth] * 0.75;
            [lifeNode setSize:CGSizeMake(lifeDimm * ratio, lifeDimm)];
            [lifeNode setPosition:CGPointMake(lifeNode.size.width * mult * neg, 0)];
            [sharedInstance addChild:lifeNode];
        }
        
        [sharedInstance reactToLivesChangedInContext:context];
    }
    
    return sharedInstance;
}

- (void)reactToLivesChangedInContext:(NSManagedObjectContext*)context
{
    User* currentUser = [User getUserInContext:context];
    float numberOfCurrentLives = currentUser.lives.integerValue;
    float numberOfTotalLives = currentUser.maxLives.integerValue;
    float numberOfLifeNodesInSelf = [self.children count] - 1; // subtract one for the fillNode
    
    assert(numberOfTotalLives == numberOfLifeNodesInSelf);
    
    float percentageToFill =  1.0 - (numberOfCurrentLives / numberOfTotalLives);
    
    [self fillNodeToPercent:percentageToFill withDuration:1.0];
}

- (BOOL)isFullWithContext:(NSManagedObjectContext*)context
{
    User* currentUser = [User getUserInContext:context];
    float numberOfCurrentLives = currentUser.lives.integerValue;
    float numberOfTotalLives = currentUser.maxLives.integerValue;
    
    if (numberOfCurrentLives == numberOfTotalLives)
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)isEmpty
{
    if (self.initialFillPlacement == kPlacementTop)
    {
        // not working yet
    }
    else if (self.initialFillPlacement == kPlacementBottom)
    {
        // not working yet
    }
    else if (self.initialFillPlacement == kPlacementLeft)
    {
        // not working yet
    }
    else // right
    {
        if (self.fillNode.position.x == 0)
        {
            return YES;
        }
    }
    
    return NO;
}

@end
