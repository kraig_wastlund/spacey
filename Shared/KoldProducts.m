//
//  KoldProducts.m
//  spacey
//
//  Created by Kraig Wastlund on 7/30/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "KoldProducts.h"

NSString* prefix = @"com.ktw.spacey.";

NSString* spaceyIAP5Buckets()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"5Buckets"];
}

NSString* spaceyIAP7Buckets()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"7Buckets"];
}

NSString* spaceyIAP7BucketsAfter5()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"7BucketsAfterFive"];
}

NSString* spaceyIAP2xRegen()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"2xRegen"];
}

NSString* spaceyIAP3xRegen()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"3xRegen"];
}

NSString* spaceyIAP3xRegenAfter2()
{
    return [NSString stringWithFormat:@"%@%@", prefix, @"3xRegenAfter2"];
}

@implementation KoldProducts

+ (NSSet*)productIdentifiers
{
    return [[NSSet alloc] initWithObjects:spaceyIAP5Buckets(), spaceyIAP7Buckets(), spaceyIAP7BucketsAfter5(), spaceyIAP2xRegen(), spaceyIAP3xRegen(), spaceyIAP3xRegenAfter2(), nil];
}

+ (IAPHelper*)store
{
    static IAPHelper* store;
    
    if (store == nil)
    {
        store = [[IAPHelper alloc] initWithProductIds:[KoldProducts productIdentifiers]];
    }
    
    return store;
}

- (NSString*)resourceNameForProductIdentifier:(NSString*)productIdentifier
{
    return [productIdentifier componentsSeparatedByString:@"."].lastObject;
}

@end


