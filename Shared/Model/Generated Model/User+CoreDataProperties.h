//
//  User+CoreDataProperties.h
//  spacey
//
//  Created by Donald Timpson on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *fuelModifier;
@property (nullable, nonatomic, retain) NSNumber *highestCompletedLevel;
@property (nullable, nonatomic, retain) NSNumber *lastPlayedLevel;
@property (nullable, nonatomic, retain) NSNumber *lives;
@property (nullable, nonatomic, retain) NSNumber *livesModifier;
@property (nullable, nonatomic, retain) NSDate *livesUpdatedOn;
@property (nullable, nonatomic, retain) NSNumber *maxLives;
@property (nullable, nonatomic, retain) NSNumber *musicOn;
@property (nullable, nonatomic, retain) NSNumber *oxygenModifier;
@property (nullable, nonatomic, retain) NSNumber *soundEffectsOn;
@property (nullable, nonatomic, retain) NSNumber *vibrateOn;
@property (nullable, nonatomic, retain) NSNumber *fps;

@end

NS_ASSUME_NONNULL_END
