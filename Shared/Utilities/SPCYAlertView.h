//
//  SPCYAlertView.h
//  spacey
//
//  Created by Kraig Wastlund on 11/21/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScene.h"

typedef enum{
    kTypeLeft,
    kTypeMiddle,
    kTypeRight,
    kTypeOk,
    kTypeCancel,
    kTypeTutorial,
}AlertButtonType;

@class SPCYAlertView;

@protocol SPCYAlertDelegate <NSObject>

@optional

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType;

@end

@interface SPCYAlertView : UIView

@property (nonatomic, assign) id<SPCYAlertDelegate> alertDelegate;
@property (nonatomic, strong) NSString* name;

- (instancetype)initWithTitle:(NSString*)title andQuery:(NSString*)query;
- (instancetype)initWithTitle:(NSString*)title query:(NSString*)query andMessage:(NSString*)message;
- (instancetype)initTutorialAlert;
- (instancetype)initWithFrame:(CGRect)frame levelNumber:(NSNumber*)levelNumber currentTime:(NSNumber*)currentTime currentMoves:(NSNumber*)currentMoves numberOfLives:(NSNumber*)numberOfLives fps:(NSNumber*)fps playerSpeed:(NSNumber*)playerSpeed level:(Level*)level andGameCode:(FinishedGameCode)gameCode plusContext:(NSManagedObjectContext*)context;

@end
