__author__ = 'Kraig Wastlund'

head = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
'''
columns_rows_moves = '''    <key>columns</key>
<integer>__COL__</integer>
<key>rows</key>
<integer>__ROW__</integer>
<key>moves</key>
<integer>__MOVES__</integer>
<key>distance</key>
<integer>__DISTANCE__</integer>
'''

start_end = '''<key>__KEY__</key>
<dict>
<key>type</key>
<integer>__TYPE__</integer>
<key>x</key>
<integer>__X__</integer>
<key>y</key>
<integer>__Y__</integer>
<key>linkId</key>
<integer>__ID__</integer>
</dict>
'''

group = '''<key>__KEY__</key>
<array>
__ITEMS__</array>
'''

items = '''        <dict>
<key>type</key>
<integer>__TYPE__</integer>
<key>x</key>
<integer>__X__</integer>
<key>y</key>
<integer>__Y__</integer>
<key>linkId</key>
<integer>__ID__</integer>
</dict>
'''

foot = '''</dict>
</plist>'''

import xlrd


def plistInfoForRows(sh):
    width = -1
    wormholes = []
    middles = []
    blackholes = []
    num_moves = 0
    distance = 0
    start = -1
    end = -1
    for rownum in range(sh.nrows):
        row_list = sh.row_values(rownum)
        if "__x__" in row_list[:4]:
            width = row_list.count("__x__")
            continue
        if "Moves:" in row_list:
            num_moves = int(row_list[row_list.index("Moves:") + 3])
        if "Distance:" in row_list:
            distance = int(row_list[row_list.index("Distance:") + 3])
        if width > 0:
            this_y = row_list[0]
            if isinstance(this_y, float):
                this_y = int(this_y)
                nodes = row_list[1:width + 2]
                for x in range(len(nodes)):
                    cell_value = nodes[x]
                    if len(cell_value) > 1:
                        if len(cell_value) > 1 and cell_value.lower()[0] == "w":
                            wormholes.append({"x": x, "y": this_y, "type": "wormhole", "linkId": cell_value[-1]})
                    elif len(cell_value) > 0:
                        if cell_value.lower()[0] == "b":
                            blackholes.append({"x": x, "y": this_y, "type": "blackhole", "linkId": -1})
                        elif cell_value.lower()[0] == "m":
                            middles.append({"x": x, "y": this_y, "type": "middle", "linkId": -1})
                        elif cell_value.lower()[0] == "s":
                            start = {"x": x, "y": this_y, "type": "start", "linkId": -1}
                        elif cell_value.lower()[0] == "e":
                            end = {"x": x, "y": this_y, "type": "end", "linkId": -1}

    return [width, width, start, end, middles, wormholes, blackholes, num_moves, distance]


def plistAsStringFromInfo(plist_info):
    width, height, start, end, middles, wormholes, blackholes, num_moves, distance = plist_info

    # head
    plist_as_string = head

    # rows, columns, moves
    temp = columns_rows_moves.replace("__COL__", str(width))
    temp = temp.replace("__ROW__", str(height))
    temp = temp.replace("__MOVES__", str(num_moves))
    temp = temp.replace("__DISTANCE__", str(distance))
    plist_as_string += temp

    # start
    temp = start_end.replace("__KEY__", "start")
    temp = temp.replace("__TYPE__", "10")
    temp = temp.replace("__X__", str(start['x']))
    temp = temp.replace("__Y__", str(start['y']))
    temp = temp.replace("__ID__", str(start['linkId']))
    plist_as_string += temp

    # end
    temp = start_end.replace("__KEY__", "end")
    temp = temp.replace("__TYPE__", "20")
    temp = temp.replace("__X__", str(end['x']))
    temp = temp.replace("__Y__", str(end['y']))
    temp = temp.replace("__ID__", str(end['linkId']))
    plist_as_string += temp

    # middles
    temp2 = ''
    for middle in middles:
        temp = items.replace("__TYPE__", "30")
        temp = temp.replace("__X__", str(middle['x']))
        temp = temp.replace("__Y__", str(middle['y']))
        temp = temp.replace("__ID__", str(middle['linkId']))
        temp2 += temp
    temp = group.replace("__KEY__", "middle")
    plist_as_string += temp.replace("__ITEMS__", temp2)

    # blackholes
    temp2 = ''
    for blackhole in blackholes:
        temp = items.replace("__TYPE__", "50")
        temp = temp.replace("__X__", str(blackhole['x']))
        temp = temp.replace("__Y__", str(blackhole['y']))
        temp = temp.replace("__ID__", str(blackhole['linkId']))
        temp2 += temp
    temp = group.replace("__KEY__", "blackHoles")
    plist_as_string += temp.replace("__ITEMS__", temp2)

    # wormholes
    temp2 = ''
    for wormhole in wormholes:
        temp = items.replace("__TYPE__", "40")
        temp = temp.replace("__X__", str(wormhole['x']))
        temp = temp.replace("__Y__", str(wormhole['y']))
        temp = temp.replace("__ID__", str(wormhole['linkId']))
        temp2 += temp
    temp = group.replace("__KEY__", "wormHoles")
    plist_as_string += temp.replace("__ITEMS__", temp2)

    # foot
    plist_as_string += foot

    return plist_as_string


def main(source_folder):
    """

    :type source_folder: str
    """
    book = xlrd.open_workbook(source_folder)
    num_converted = 0
    for i in range(len(book._sheet_list)):
        sh = book.sheet_by_index(i)

        plistName = "Level{}.plist".format(sh.name)
        outputPath = "./GeneratedLevels/"

        plist_info = plistInfoForRows(sh)
        if (plist_info[2] == -1 or plist_info[3] == -1) or sh.name[0] not in "1234567890":
            print "Sheet at index:{0} with the name:{1} not converted".format(i + 1, sh.name)
            continue

        num_converted += 1
        plist_string = plistAsStringFromInfo(plist_info)

        fout = open(outputPath + plistName, "wt")
        fout.write(plist_string)
        fout.close()

    print
    print "##################################################################"
    print "  {} Levels successfully generated in the GeneratedLevels folder.".format(num_converted)
    print "##################################################################"
    print
    return


source_folder = "./DS/levels.xlsx"

main(source_folder)