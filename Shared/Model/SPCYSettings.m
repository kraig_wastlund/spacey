//
//  SPCYSettings.m
//  spacey
//
//  Created by Kraig Wastlund on 11/18/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SPCYSettings.h"
#import <UIKit/UIKit.h>
#import "User.h"

@implementation SPCYSettings


#pragma mark - Game Over Messages

+ (NSString*)soundFileForMessage:(NSString*)message {
    int rand = arc4random() % 3;
    
    if ([message isEqualToString:@"You are victorious! Stay tuned, more levels to come..."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"You are victorious! Stay tuned, more levels to come..."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Way to go, you're the best! (NOT)"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"SMILEYFACE"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"17th times the charm they say!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Three moons! Check the tide."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"How many moons does it take to change a light?"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Of all the losers, you are the best!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Two moons! You are completely mediocre."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Being average is almost above average!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Wow, I'm stunned."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Um. That was painful."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Watch out, this person means business!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Well, at least you can move on."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Hooray.  You can be taught!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"How many times did that take you?"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Um, I won't tell anyone if you don't."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"You barely won, I mean BARELY!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Wow, you really stink."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"I'm surprised you can turn on a phone."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"That was kind of embarrassing."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Well, there's always next time... Maybe"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"It's not me....\nit's you."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"You lose, because you quit!"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"You are out of juice."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Seriously, you can't move."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Basically you're just waiting to die..."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Ho Hum"]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"Space.... the spinal front sphere."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"I'm getting cold."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else if ([message isEqualToString:@"I think I just saw Halley's comet."]) {
        if (rand == 0) {
            return @"";
        } else if (rand == 1) {
            return @"";
        } else {
            return @"";
        }
    } else {
        
    }
    
    return @"";
}

+ (NSString*)gameCompletedMessage
{
    NSArray* messageArray = @[
                              @"You are victorious! Stay tuned, more levels to come...",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)threeMoonsMessage
{
    NSArray* messageArray = @[
                              @"Way to go, you're the best! (NOT)",
                              @"SMILEYFACE",
                              @"17th times the charm they say!",
                              @"Three moons! Check the tide.",
                              @"How many moons does it take to change a light?",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)twoMoonsMessage
{
    NSArray* messageArray = @[
                              @"Of all the losers, you are the best!",
                              @"Two moons! You are completely mediocre.",
                              @"Being average is almost above average!",
                              @"Wow, I'm stunned.",
                              @"Um. That was painful.",
                              @"Watch out, this person means business!",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)oneMoonsMessage
{
    NSArray* messageArray = @[
                              @"Well, at least you can move on.",
                              @"Hooray.  You can be taught!",
                              @"How many times did that take you?",
                              @"Um, I won't tell anyone if you don't.",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)zeroMoonsMessage
{
    NSArray* messageArray = @[
                              @"You barely won, I mean BARELY!",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)gameLostMessage
{
    NSArray* messageArray = @[
                              @"Wow, you really stink.",
                              @"I'm surprised you can turn on a phone.",
                              @"That was kind of embarrassing.",
                              @"Well, there's always next time... Maybe",
                              @"It's not me....\nit's you.",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)gameQuitMessage
{
    NSArray* messageArray = @[
                              @"You lose, because you quit!",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}

+ (NSString*)outOfGasMessage
{
    NSArray* messageArray = @[
                              @"You are out of juice.",
                              @"Seriously, you can't move.",
                              @"Basically you're just waiting to die...",
                              @"Ho Hum",
                              @"Space.... the spinal front sphere.",
                              @"I'm getting cold.",
                              @"I think I just saw Halley's comet.",
                              ];
    
    return [messageArray objectAtIndex:arc4random() % [messageArray count]];
}


#pragma mark - Color Arrays

+ (NSArray*)wormHoleColors
{
    return @[
             [SPCYSettings colorFromHexString:@"ff00ff"],
             [SPCYSettings colorFromHexString:@"00ffff"],
             [SPCYSettings colorFromHexString:@"00ff00"],
             [SPCYSettings colorFromHexString:@"ffff00"],
             [SPCYSettings colorFromHexString:@"ff0000"],
             [SPCYSettings colorFromHexString:@"0000ff"],
             [SPCYSettings colorFromHexString:@"7920ff"],
             [SPCYSettings colorFromHexString:@"fd0987"],
             [SPCYSettings colorFromHexString:@"ff3300"],
             ];
}

+ (NSArray*)planetColors
{
    return @[
             [UIColor colorWithRed:1 green:0 blue:0 alpha:1],
             [UIColor colorWithRed:0.25 green:0 blue:1 alpha:1],
             [UIColor colorWithRed:1 green:0.25 blue:0 alpha:1],
             [UIColor colorWithRed:1 green:0.5 blue:0 alpha:1],
             [UIColor colorWithRed:1 green:0.75 blue:0 alpha:1],
             [UIColor colorWithRed:1 green:1 blue:0 alpha:1],
             [UIColor colorWithRed:0.75 green:1 blue:0 alpha:1],
             [UIColor colorWithRed:0.5 green:1 blue:0 alpha:1],
             [UIColor colorWithRed:0.25 green:1 blue:0 alpha:1],
             [UIColor colorWithRed:0 green:1 blue:0 alpha:1],
             [UIColor colorWithRed:0 green:1 blue:0.25 alpha:1],
             [UIColor colorWithRed:0 green:1 blue:0.5 alpha:1],
             [UIColor colorWithRed:0 green:1 blue:0.75 alpha:1],
             [UIColor colorWithRed:0 green:1 blue:1 alpha:1],
             [UIColor colorWithRed:0 green:0.75 blue:1 alpha:1],
             [UIColor colorWithRed:0 green:0.5 blue:1 alpha:1],
             [UIColor colorWithRed:0 green:0.25 blue:1 alpha:1],
             [UIColor colorWithRed:0 green:0 blue:1 alpha:1],
             [UIColor colorWithRed:0.5 green:0 blue:1 alpha:1],
             [UIColor colorWithRed:0.75 green:0 blue:1 alpha:1],
             [UIColor colorWithRed:1 green:0 blue:1 alpha:1],
             [UIColor colorWithRed:1 green:0 blue:0.75 alpha:1],
             [UIColor colorWithRed:1 green:0 blue:0.5 alpha:1],
             [UIColor colorWithRed:1 green:0 blue:0.25 alpha:1],
             ];
}


#pragma mark - SPCY Colors

+ (UIColor*)spcyBlueColor
{
    return [SPCYSettings colorFromHexString:@"27DEE7"];
}

+ (UIColor*)spcyYellowColor
{
    return [SPCYSettings colorFromHexString:@"FFE500"];
}

+ (UIColor*)spcyPinkColor
{
    return [SPCYSettings colorFromHexString:@"FF378A"];
}

+ (UIColor*)spcyGreenColor
{
    return [SPCYSettings colorFromHexString:@"10ff73"];
}

+ (UIColor*)spcyDisabledColor
{
    return [SPCYSettings colorFromHexString:@"a8a8a8"];
}

+ (UIColor*)spcyWhiteColor
{
    return [SPCYSettings colorFromHexString:@"ffffff"];
}

+ (UIColor*)spcyGrayColor
{
    return [SPCYSettings colorFromHexString:@"cccccc"];
}

+ (UIColor*)spcyDisabledOverlayColor
{
    return [SPCYSettings colorFromHexString:@"696969"];
}


#pragma mark - Color Helper

+ (UIColor*)colorFromHexString:(NSString*)hexString
{
    if ([hexString isEqualToString:@"------"])
    {
        return [UIColor clearColor];
    }
    
    unsigned rgbValue = 0;
    NSScanner* scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16) / 255.0 green:((rgbValue & 0xFF00) >> 8) / 255.0 blue:(rgbValue & 0xFF) / 255.0 alpha:1.0];
}


#pragma mark - Lives

+ (float)lifeNodeWidth
{
    return 20.0;
}


#pragma mark - Toggle

+ (BOOL)spcyDebugToggle
{
    NSString* playerPath = [[NSBundle mainBundle] pathForResource:@"Player" ofType:@"plist"];
    NSDictionary* playerDictionary = [NSDictionary dictionaryWithContentsOfFile:playerPath];
    
    return [[playerDictionary objectForKey:@"debugToggle"] boolValue];
}

@end
