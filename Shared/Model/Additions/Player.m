//
//  Player+Additions.m
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "Player.h"
#import "Level.h"
#import "SPCYSettings.h"

static const uint32_t playerCategory        =  0x1 << 0;
static const uint32_t middleCategory        =  0x1 << 1;
static const uint32_t wormHoleCategory      =  0x1 << 2;
static const uint32_t blackHoleCategory     =  0x1 << 3;
static const uint32_t endCategory           =  0x1 << 4;

@interface Player()

@property (strong, nonatomic) NSString* filePath;

@end

@implementation Player

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.maxNumberOfLives = @3;
        self.currentNumberOfLives = @3;
    }
    
    return self;
}

+ (Player*)getPlayerForLevel:(Level*)level
{
    Player* newPlayer = [[Player alloc] init];
    NSString* playerPath = [[NSBundle mainBundle] pathForResource:@"Player" ofType:@"plist"];
    NSDictionary* playerDictionary = [NSDictionary dictionaryWithContentsOfFile:playerPath];
    
    newPlayer.speed = [(NSNumber*)[playerDictionary objectForKey:@"speed"] doubleValue];
    newPlayer.currentLevel = [playerDictionary objectForKey:@"currentLevel"];
    newPlayer.filePath = playerPath;
    newPlayer.name = @"player";
    newPlayer.size = [Player scalePlayerWithCellSize:level.cellSize];
    
    [newPlayer setPosition:level.start.position];
    [newPlayer setCurrentNode:level.start];
    [newPlayer setZPosition:1000];
    newPlayer.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:newPlayer.size.height * 0.20];
    newPlayer.physicsBody.dynamic = YES;
    newPlayer.physicsBody.categoryBitMask = playerCategory;
    newPlayer.physicsBody.contactTestBitMask = endCategory | middleCategory | blackHoleCategory | wormHoleCategory;
    newPlayer.physicsBody.affectedByGravity = NO;
    
    SKAction* hover = [SKAction sequence:@[
                                           [SKAction scaleTo:0.9 duration:1.0],
                                           [SKAction rotateToAngle:-0.1 * M_PI / 12 duration:1.0],
                                           [SKAction scaleTo:1.0 duration:1.0],
                                           [SKAction scaleTo:0.9 duration:1.0],
                                           [SKAction rotateToAngle:0.1 * M_PI / 12 duration:1.0],
                                           [SKAction scaleTo:1.0 duration:1.0],
                                           ]];
    [newPlayer runAction:[SKAction repeatActionForever:hover] withKey:@"hover"];
    
    [newPlayer setTexture:[SKTexture textureWithImageNamed:@"spaceyship"]];
    
    //    [newPlayer runAction:[SKAction repeatActionForever:[SKAction playSoundFileNamed:@"spaceyship_idol" waitForCompletion:YES]]];
    
    return newPlayer;
}

+ (CGSize)scalePlayerWithCellSize:(CGSize)cellSize
{
    CGSize playerSize;
    
    CGFloat cellWidth = cellSize.width;
    CGFloat playerWidth = cellWidth * 2.0;
    CGFloat playerHeight = playerWidth * 0.63;
    playerSize.width = playerWidth;
    playerSize.height = playerHeight;
    
    return playerSize;
}

- (void)playRev
{
//    [self runAction:[SKAction playSoundFileNamed:@"spaceyship_rev.wav" waitForCompletion:YES]];
}

- (void)savePlayer
{
    NSDictionary* playerDict = @{ @"speed" : [NSNumber numberWithDouble:self.speed],
                                  @"currentLevel" : self.currentLevel };
    
    [playerDict writeToFile:self.filePath atomically:YES];
}

+ (NSArray*)pointsForCircleFromPoint:(CGPoint)fromPoint toPoint:(CGPoint)toPoint usingOrigin:(CGPoint)origin
{
    NSMutableArray* allPoints = [[NSMutableArray alloc] init];
    
    double numberOfPoints = fabs(fromPoint.x - toPoint.x) / 6 - 2;
    double xInc = (toPoint.x - fromPoint.x) / numberOfPoints;
    double radiusSquare = (toPoint.x - origin.x)*(toPoint.x - origin.x) + (toPoint.y - origin.y)*(toPoint.y - origin.y);
    
    CGFloat x = fromPoint.x;
    CGFloat y = fromPoint.y;
    
    if (((toPoint.y + fromPoint.y)/2) > origin.y)
    {
        for (int i = 0; i < numberOfPoints; i++)
        {
            x += xInc;
            y = sqrt(radiusSquare - (x - origin.x)*(x - origin.x)) + origin.y;
            
            if (isnan(x) || isnan(y))
            {
                NSLog(@"problem NAN");
            }
            else
            {
                [allPoints addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
            }
        }
        
        [allPoints addObject:[NSValue valueWithCGPoint:toPoint]];
    }
    else
    {
        for (int i = 0; i < numberOfPoints; i++)
        {
            x += xInc;
            y = -sqrt(radiusSquare - (x - origin.x)*(x - origin.x)) + origin.y;
            
            if (isnan(x) || isnan(y))
            {
                NSLog(@"problem NAN");
            }
            else
            {
                [allPoints addObject:[NSValue valueWithCGPoint:CGPointMake(x, y)]];
            }
        }
        
        [allPoints addObject:[NSValue valueWithCGPoint:toPoint]];
    }
    
    return allPoints;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_0000%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_000%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    
    return imageName;
}

+ (NSNumber*)getPlayersCurrentLevelFromPlist // talking about the plist in the documents directory that is mutable
{
    NSString* playerPath = [[NSBundle mainBundle] pathForResource:@"Player" ofType:@"plist"];
    NSDictionary* playerDictionary = [NSDictionary dictionaryWithContentsOfFile:playerPath];
    
    return [playerDictionary objectForKey:@"currentLevel"];
}

+ (void)setPlayersPlistToLevel:(NSNumber*)levelNumber // talking about the plist in the documents directory that is mutable
{
    NSString* plistPath = [(NSString*)[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/Player.plist"];
    NSMutableDictionary* playerDict = [[NSDictionary dictionaryWithContentsOfFile:plistPath] mutableCopy];
    
    NSNumber* levelOnPlist = [playerDict objectForKey:@"currentLevel"];
    
    if  (levelNumber.integerValue > levelOnPlist.integerValue)
    {
        [playerDict setObject:levelNumber forKey:@"currentLevel"];
        [playerDict writeToFile:plistPath atomically:YES];
    }
}

@end