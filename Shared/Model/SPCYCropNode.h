//
//  SPCYCropNode.h
//  spacey
//
//  Created by Kraig Wastlund on 12/15/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

typedef enum {
    kPlacementTop,
    kPlacementBottom,
    kPlacementLeft,
    kPlacementRight
}SPCYPlacement;

#import <SpriteKit/SpriteKit.h>

@interface SPCYCropNode : SKSpriteNode

@property (nonatomic, strong) SKSpriteNode* fillNode;
@property (nonatomic) SPCYPlacement initialFillPlacement;

- (instancetype)initCropNodeWithFrameImageName:(NSString*)imageName maskImageName:(NSString*)maskName fillObject:(id)fillObject size:(CGSize)size initialFillPlacement:(SPCYPlacement)fillPlacement;
- (void)fillNodeToPercent:(CGFloat)percent withDuration:(CGFloat)duration;

@end
