//
//  MainMenu.h
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SPCYLivesNode.h"

@interface MainScene : SKScene

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (void)reactToTimer;
- (void)updateLives;

@end
