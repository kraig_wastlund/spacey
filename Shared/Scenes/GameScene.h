//
//  GameScene.h
//  code_camp_2015
//

//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Level.h"

typedef enum {
    kGameWon,
    kGameLost,
    kGameQuit,
} FinishedGameCode;

@interface GameScene : SKScene

@property (nonatomic, strong) NSNumber* levelNumber;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
