//
//  GameViewController.m
//  code_camp_2015
//
//  Created by Kraig Wastlund on 11/12/15.
//  Copyright (c) 2015 Kraig. All rights reserved.
//

#import "GameViewController.h"
#import "MainScene.h"
#import "GameScene.h"
#import "LevelsScene.h"
#import "SoundController.h"
#import "SPCYSettings.h"
@import AVFoundation;
#import "User.h"

@interface GameViewController()

@property (nonatomic, strong) SKView* skView;
@property (nonatomic) AVAudioPlayer* backgroundMusicPlayer;

@end

@implementation GameViewController

- (void)awakeFromNib
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.skView = (SKView*)self.view;
    self.skView.ignoresSiblingOrder = YES;
    [self.skView setBackgroundColor:[UIColor clearColor]];
    
    NSError *error;
    NSURL * backgroundMusicURL = [[NSBundle mainBundle] URLForResource:@"background_music" withExtension:@"mp3"];
    self.backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    self.backgroundMusicPlayer.numberOfLoops = -1;
//    [self.backgroundMusicPlayer prepareToPlay];
    [self.backgroundMusicPlayer setVolume:0.4];
//    [self.backgroundMusicPlayer play];
    
    MainScene* mainMenu = [[MainScene alloc] initWithSize:self.view.frame.size];
    [self.skView presentScene:mainMenu];
}

- (void)debuggingMode
{
    self.skView.showsFPS = YES;
    self.skView.showsDrawCount = YES;
    self.skView.showsNodeCount = YES;
    self.skView.showsQuadCount = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // coming back from settings
}

@end
