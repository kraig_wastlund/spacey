//
//  AboutViewController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/14/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "AboutViewController.h"
#import "SPCYSettings.h"

@interface AboutViewController ()

@property (nonatomic, strong) NSArray* dataSource;
@property (nonatomic) NSInteger cellLabelWidth;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set nav bar title to be white
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"< Back" style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed:)];
    [backButton setTintColor:[UIColor whiteColor]];
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 20)];
    
    self.cellLabelWidth = self.tableView.frame.size.width - 32;
    
    [self setupDataSource];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForCellAtRow:indexPath.row];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.userInteractionEnabled = NO;
    }
    
    [self configureCell:cell withType:[[self.dataSource objectAtIndex:indexPath.row] objectForKey:@"type"] andValue:[[self.dataSource objectAtIndex:indexPath.row] objectForKey:@"value"]];
    
    return cell;
}

- (void)configureCell:(UITableViewCell*)cell withType:(NSString*)type andValue:(NSString*)value
{
    [cell.textLabel setText:value];
    [cell.textLabel sizeToFit];
    [cell.textLabel setTextColor:[SPCYSettings spcyBlueColor]];
    
    if ([type isEqualToString:@"t1"])
    {
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.textLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightRegular]];
        [cell.textLabel setNumberOfLines:1];
        [cell.textLabel setLineBreakMode:NSLineBreakByClipping];
    }
    else if ([type isEqualToString:@"p1"])
    {
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.textLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight]];
        [cell.textLabel setNumberOfLines:0];
        [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    }
}

- (NSInteger)heightForCellAtRow:(NSInteger)row
{
    NSString* text = [[self.dataSource objectAtIndex:row] objectForKey:@"value"];
    NSString* type = [[self.dataSource objectAtIndex:row] objectForKey:@"type"];
    if ([type isEqualToString:@"t1"])
    {
        return 20;
    }
    
    NSInteger height = 0;
    height += ceilf(CGRectGetHeight([text boundingRectWithSize:CGSizeMake(self.cellLabelWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight]} context:nil]));
    
    height += 10; // padding
    
    return height;
}

- (void)setupDataSource
{
    self.dataSource = @[
                        @{@"type" : @"t1", @"value" : @"Republic Credits"},
                        @{@"type" : @"p1", @"value" : @"Bit Quest Kevin MacLeod (incompetech.com) Licensed under Creative Commons: By Attribution 3.0 License http://creativecommons.org/licenses/by/3.0/"},
                        @{@"type" : @"p1", @"value" : @""},
                        @{@"type" : @"p1", @"value" : @"Bradyn the Wise for sitting next to me while I type this."},
                        @{@"type" : @"p1", @"value" : @""},
                        @{@"type" : @"t1", @"value" : @"koldapps.com"},
                        ];
}

#pragma mark - actions

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
