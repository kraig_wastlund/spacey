//
//  FlyingShip.m
//  spacey
//
//  Created by Kraig Wastlund on 11/27/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "FlyingShipNode.h"

@interface FlyingShipNode ()


@property (nonatomic, strong) NSArray* shipFlyingFrames;

@end

@implementation FlyingShipNode

+ (instancetype)flyingShipNodeWithFrame:(CGRect)frame
{
    FlyingShipNode* ship;
    
    NSArray* shipFrames = [FlyingShipNode setupShipFlyingFrames];
    
    SKTexture* firstFrame = [shipFrames objectAtIndex:0];
    ship = (FlyingShipNode*)[SKSpriteNode spriteNodeWithTexture:firstFrame];
    ship.size = CGSizeMake(frame.size.width, frame.size.width);
    ship.position = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    ship.zPosition = 1;
    
    [ship runAction:[SKAction repeatActionForever:
                          [SKAction animateWithTextures:shipFrames
                                           timePerFrame:0.025f
                                                 resize:NO
                                                restore:YES]] withKey:@"FlyingShip"];
    
    return ship;
}

+ (NSArray*)setupShipFlyingFrames
{
    NSMutableArray* frames = [[NSMutableArray alloc] init];
    NSInteger frameCount = 119;
    
    for (int i = 0; i < frameCount; i++)
    {
        NSString* imageName = [[NSString alloc] init];
        
        if (i < 10)
        {
            imageName = [NSString stringWithFormat:@"Comp 2_0000%d", i];
        }
        else if (i<100)
        {
            imageName = [NSString stringWithFormat:@"Comp 2_000%d", i];
        }
        else
        {
            imageName = [NSString stringWithFormat:@"Comp 2_00%d", i];
        }
        
        SKTexture* tmpTexture = [SKTexture textureWithImage:[UIImage imageNamed:imageName]];
        [frames addObject:tmpTexture];
        
    }
    
    return [[NSArray alloc] initWithArray:frames];
    
}


@end
