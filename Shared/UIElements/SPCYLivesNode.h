//
//  SPCYLivesNode.h
//  spacey
//
//  Created by Kraig Wastlund on 5/13/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "SPCYCropNode.h"

@interface SPCYLivesNode : SPCYCropNode

+ (SPCYLivesNode*)sharedLivesNodeInContext:(NSManagedObjectContext*)context;
- (void)reactToLivesChangedInContext:(NSManagedObjectContext*)context;
- (BOOL)isFullWithContext:(NSManagedObjectContext*)context;
- (BOOL)isEmpty;

@end
