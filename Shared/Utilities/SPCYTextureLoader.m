//
//  SPCYTextureLoader.m
//  spacey
//
//  Created by Chad Olsen on 5/16/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SPCYTextureLoader.h"

@implementation SPCYTextureLoader

+ (void)loadPlanetTextureToArray:(NSArray*)loadToArray
{
    SKTextureAtlas *planetTexture = [SKTextureAtlas atlasNamed:@"end"];
    NSMutableArray * rotateFrames = [[NSMutableArray alloc] init];
    NSInteger numImages = planetTexture.textureNames.count;
    for(int i = 0; i < numImages; i++)
    {
        NSString* name = @"end";
        NSString* fileName = [SPCYTextureLoader fileNameForName:name andIndex:i];
        [rotateFrames addObject:[SKTexture textureWithImageNamed:fileName]];
    }
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
