//
//  MusicHelper.swift
//  spacey
//
//  Created by Kraig Wastlund on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

import AVFoundation

class MusicHelper {
    static let sharedHelper = MusicHelper()
    var audioPlayer: AVAudioPlayer?
    
    func playBackgroundMusic() {
        if audioPlayer?.playing == true {
            return
        }
        
        let aSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("background_music", ofType: "mp3")!)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL:aSound)
            audioPlayer!.numberOfLoops = -1
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        } catch {
            print("Cannot play the file")
        }
    }
    
    func stopPlaying() -> () {
        audioPlayer?.stop()
    }
}
