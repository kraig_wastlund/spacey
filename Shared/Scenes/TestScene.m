//
//  TestScene.m
//  spacey
//
//  Created by Kraig Wastlund on 12/12/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "TestScene.h"
#import "SPCYCropNode.h"

@implementation TestScene

- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self)
    {
        [self setSize:size];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    
    return self;
}

- (void)didMoveToView:(SKView*)view
{
    [self setupBackground];
    
    [self setupCrop];
}

- (void)setupBackground
{
    SKSpriteNode *backgroundNode = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    backgroundNode.size = self.frame.size;
    backgroundNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundNode.name = @"BACKGROUND";
    [self addChild:backgroundNode];
    [backgroundNode setZPosition:-10];
}

- (void)setupCrop
{
    SPCYCropNode* fuelGuage = [[SPCYCropNode alloc] initCropNodeWithFrameImageName:@"fuelFrame" maskImageName:@"fuelMask" fillObject:@"fuelFill" size:CGSizeMake(200, 100) initialFillPlacement:kPlacementLeft];
    [fuelGuage setPosition:CGPointMake(self.size.width / 2, 200)];
    [self addChild:fuelGuage];
    
    [fuelGuage fillNodeToPercent:.75 withDuration:1];
}

@end
