//
//  SPCYCropNode.m
//  spacey
//
//  Created by Kraig Wastlund on 12/15/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SPCYCropNode.h"
#import "SPCYSettings.h"

@interface SPCYCropNode()

@end

@implementation SPCYCropNode

- (instancetype)initCropNodeWithFrameImageName:(NSString*)imageName maskImageName:(NSString*)maskName fillObject:(id)fillObject size:(CGSize)size initialFillPlacement:(SPCYPlacement)fillPlacement
{
    self = [super initWithImageNamed:imageName];
    
    if (self)
    {
        self.initialFillPlacement = fillPlacement;
        
        SKSpriteNode* fill;
        if ([fillObject isKindOfClass:[UIColor class]])
        {
            fill = [SKSpriteNode spriteNodeWithColor:fillObject size:size];
        }
        else
        {
            fill = [SKSpriteNode spriteNodeWithImageNamed:fillObject];
        }
        if (fillPlacement == kPlacementTop)
        {
            [fill setPosition:CGPointMake(0, +fill.size.height)];
        }
        else if (fillPlacement == kPlacementBottom)
        {
            [fill setPosition:CGPointMake(0, -fill.size.height)];
        }
        else if (fillPlacement == kPlacementLeft)
        {
            [fill setPosition:CGPointMake(-fill.size.width, 0)];
        }
        else // right
        {
            [fill setPosition:CGPointMake(+fill.size.width, 0)];
        }
        fill.name = @"fill";
        self.fillNode = fill;
        
        SKSpriteNode* mask = [SKSpriteNode spriteNodeWithImageNamed:maskName];
        
        CGSize fillSize;
        if ([maskName containsString:@"default"]) // base size off of size passed in
        {
            fillSize = size;
        }
        else // base size off of mask ratio
        {
            CGFloat widthRatio = mask.size.width / self.size.width;
            CGFloat heightRatio = mask.size.height / self.size.height;
            fillSize = CGSizeMake(size.width * widthRatio, size.height * heightRatio);
        }
        
        [mask setSize:fillSize];
        [fill setSize:fillSize];
        
        SKCropNode* crop = [SKCropNode node];
        crop.name = @"crop";
        [crop setZPosition:10];
        [crop addChild:fill];
        [crop setMaskNode:mask];
        
        
        [self setSize:size];
        [self addChild:crop];
    }
    
    return self;
}

- (void)fillNodeToPercent:(CGFloat)percent withDuration:(CGFloat)duration
{    
    if (self.initialFillPlacement == kPlacementTop)
    {
        CGFloat pos = 0 - (self.fillNode.size.height - percent * self.fillNode.size.height);
        [self.fillNode runAction:[SKAction moveToY:pos duration:duration]];
    }
    else if (self.initialFillPlacement == kPlacementBottom)
    {
        CGFloat pos = 0 + (self.fillNode.size.height - percent * self.fillNode.size.height);
        [self.fillNode runAction:[SKAction moveToY:pos duration:duration]];
    }
    else if (self.initialFillPlacement == kPlacementLeft)
    {
        CGFloat pos = 0 - (self.fillNode.size.width - percent * self.fillNode.size.width);
        [self.fillNode runAction:[SKAction moveToX:pos duration:duration]];
    }
    else // right
    {
        CGFloat pos = 0 + (self.fillNode.size.width - percent * self.fillNode.size.width);
        [self.fillNode runAction:[SKAction moveToX:pos duration:duration]];
    }
}

@end
