//
//  LevelMenu.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "LevelsScene.h"
#import "Level.h"
#import "Player.h"
#import "MenuPlanet.h"
#import "SoundController.h"
#import "GameScene.h"
#import "MainScene.h"
#import "SPCYSettings.h"
#import "User.h"
#import "SPCYConstants.h"
#import "SPCYTextureLoader.h"

@interface LevelsScene ()

@property (nonatomic, strong) SKSpriteNode* background;
@property (nonatomic, strong) SKSpriteNode* backButton;
@property (nonatomic, strong) NSMutableArray* levelButtonArray;
@property (nonatomic, strong) NSArray* colorsArray;
@property (nonatomic, strong) SKNode* fullView;
@property (nonatomic) BOOL isSwiping;
@property (nonatomic) NSInteger deltaY;

@property (nonatomic) CGFloat btnHeight;
@property (nonatomic) CGFloat btnWidth;
@property (nonatomic) CGFloat spacing;

@property (nonatomic, strong) User* user;

@property (nonatomic, strong) NSArray* planetTextures;

@end


@implementation LevelsScene

-(void)didMoveToView:(SKView *)view
{
    self.user = [User getUserInContext:self.managedObjectContext];
    SKShapeNode* navBar = [SKShapeNode shapeNodeWithRect:CGRectMake(0, self.view.frame.size.height - 80, self.view.frame.size.width, 80)];
    [navBar setFillColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    [navBar setStrokeColor:[UIColor clearColor]];
    [self addChild:navBar];
    [navBar setZPosition:100];
    
    self.btnHeight = 50;
    self.btnWidth = 50;
    self.spacing = 75;
    self.levelButtonArray = [[NSMutableArray alloc] init];
    self.colorsArray = [SPCYSettings planetColors];
    
    NSArray* tmpArray = [[NSArray alloc] init];
    
    [SPCYTextureLoader loadPlanetTextureToArray:tmpArray];
    
    self.planetTextures = tmpArray;
    
    CGFloat yOffset = self.btnHeight + 10;
    NSInteger count = 1;
    
    self.deltaY = 0;
    
    self.background = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    self.background.size = self.frame.size;
    self.background.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    self.background.zPosition = 0;
    [self addChild:self.background];
    
    self.fullView = [[SKNode alloc] init];
    self.fullView.position = CGPointMake(0, 0);
    [self addChild:self.fullView];
    
    NSInteger numberOfLevels = [Level getTotalNumberOfLevels];
    CGFloat xOffset = CGRectGetMidX(self.frame) / 2 * .66;
    
    for (int i = 1; i <= numberOfLevels; i++)
    {
        MenuPlanet* levelButton = [[MenuPlanet alloc] init];
        
        if (count%2 == 0)
        {
            levelButton.position = CGPointMake(CGRectGetMidX(self.frame) - xOffset, CGRectGetMinY(self.frame) + yOffset);
        }
        else
        {
            levelButton.position = CGPointMake(CGRectGetMidX(self.frame) + xOffset, CGRectGetMinY(self.frame) + yOffset);
        }

        levelButton.name = [NSString stringWithFormat:@"%d", i];
        levelButton.levelNumber = [NSNumber numberWithInt:i];
        levelButton.zPosition = 3;
        levelButton.worldColor = [self.colorsArray objectAtIndex:i % self.colorsArray.count];
        if (i > [[User getUserInContext:self.managedObjectContext].highestCompletedLevel integerValue] + 1)
        {
            levelButton.isDark = YES;
        }
        else
        {
            levelButton.isDark = NO;
        }
        
        [levelButton setupWorldNode];
        
        
        [self.fullView addChild:levelButton];
        [self.levelButtonArray addObject:levelButton];
        
        yOffset += self.btnHeight + self.spacing;
        
        if (i != numberOfLevels)
        {
            SKSpriteNode* path = [SKSpriteNode spriteNodeWithImageNamed:@"level_path"];
            path.size = CGSizeMake(xOffset * 2, self.btnHeight + self.spacing);
            path.position = CGPointMake(CGRectGetMidX(self.frame), yOffset - (self.btnHeight + self.spacing)/2);
            
            if (count%2 == 0)
            {
                path.yScale = -1.0;
            }
            path.zPosition = 2;
            [self.fullView addChild:path];
        }
        
        count++;
    }
    
    //Add button 1
    self.backButton = [SKSpriteNode spriteNodeWithImageNamed:@"back"];
    self.backButton.size = CGSizeMake(50, 50);
    self.backButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame) - (self.btnHeight/2) - 20);
    [self addChild:self.backButton];
    [self.backButton setZPosition:120];
}

- (void)willMoveFromView:(SKView *)view
{
    [self removeActionsFromView];
    [self removeAllChildren];
    [self removeAllActions];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    
    CGPoint tapLocationForBackButton = [touch locationInNode:self];
    
    if ([[self nodeAtPoint:tapLocationForBackButton] isEqualToNode:self.backButton])
    {
        [self playClick];
        
        MainScene* main = [[MainScene alloc] initWithSize:self.view.frame.size];
        main.managedObjectContext = self.managedObjectContext;
        [self.view presentScene:main transition:[SKTransition crossFadeWithDuration:1.0]];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    // 2 Get touch location
    UITouch* touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:self];
    CGPoint previousLocation = [touch previousLocationInNode:self];
    
    CGFloat swipeDelta = touchLocation.y - previousLocation.y;
    if (fabs(swipeDelta) > 2)
    {
        self.isSwiping = YES;
        self.deltaY = swipeDelta;
    }
    else
    {
        [self.fullView removeActionForKey:@"swiping"];
        self.isSwiping = NO;
    }
}

- (void)touchesEnded:(NSSet<UITouch*>*)touches withEvent:(UIEvent*)event
{
    if (!self.isSwiping)
    {
        if (self.deltaY != 0)
        {
            self.deltaY = 0;
        }
        else if ([User getUserInContext:self.managedObjectContext].lives.integerValue > 0)
        {
            UITouch* touch = [touches anyObject];
            
            CGPoint location = [touch locationInNode:self.fullView];
            
            for (MenuPlanet* levelNode in self.levelButtonArray)
            {
                if ([levelNode containsPoint:location])
                {
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterDecimalStyle;
                    NSNumber* levelNumber = [f numberFromString:levelNode.name];
                    if ([levelNumber isEqualToNumber:@1] || levelNumber.integerValue <= [User getUserInContext:self.managedObjectContext].highestCompletedLevel.integerValue + 1)
                    {
                        [self playClick];
                        
                        GameScene* game = [[GameScene alloc] initWithSize:self.view.frame.size];
                        game.levelNumber = levelNumber;
                        game.managedObjectContext = self.managedObjectContext;
                        [self.view presentScene:game transition:[SKTransition crossFadeWithDuration:0.5]];
                    }
                    break;
                }
            }
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Out of Lives" message:@"You can purchase more lives at the store." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

-(void)update:(CFTimeInterval)currentTime
{
//    if (!self.isSwiping)
//    {
//        self.deltaY = 0;
//    }
    /* Called before each frame is rendered */
    CGRect frame = self.fullView.calculateAccumulatedFrame;
    NSInteger stoppingPoint = frame.size.height - self.view.frame.size.height * 0.8;
    if (self.fullView.position.y > 0)
    {
        [self.fullView setPosition:CGPointMake(self.fullView.position.x, 0)];
        self.isSwiping = NO;
        self.deltaY = 0;
    }
    else if (self.fullView.position.y < -stoppingPoint)
    {
        [self.fullView setPosition:CGPointMake(self.fullView.position.x, -stoppingPoint)];
        self.isSwiping = NO;
        self.deltaY = 0;
    }
    
    if (self.deltaY != 0)
    {
        [self.fullView setPosition:CGPointMake(self.fullView.position.x, self.fullView.position.y + self.deltaY)];
    }
}

-(void)didSimulatePhysics
{
    for (MenuPlanet* i in self.levelButtonArray)
    {
        [i movePlanetNode];
    }
}

- (void)removeActionsFromView
{
    for (MenuPlanet* world in self.levelButtonArray)
    {
        [world removeMenuPlanet];
    }
}


#pragma mark - Sounds

- (void)playClick
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringClick waitForCompletion:NO]];
    }
}

@end
