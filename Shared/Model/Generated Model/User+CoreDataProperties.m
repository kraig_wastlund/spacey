//
//  User+CoreDataProperties.m
//  spacey
//
//  Created by Donald Timpson on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic fuelModifier;
@dynamic highestCompletedLevel;
@dynamic lastPlayedLevel;
@dynamic lives;
@dynamic livesModifier;
@dynamic livesUpdatedOn;
@dynamic maxLives;
@dynamic musicOn;
@dynamic oxygenModifier;
@dynamic soundEffectsOn;
@dynamic vibrateOn;
@dynamic fps;

@end
