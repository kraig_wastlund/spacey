//
//  SPCYSettings.h
//  spacey
//
//  Created by Kraig Wastlund on 11/18/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SPCYSettings : NSObject

+ (NSString*)gameCompletedMessage;
+ (NSString*)gameLostMessage;
+ (NSString*)gameQuitMessage;
+ (NSString*)threeMoonsMessage;
+ (NSString*)twoMoonsMessage;
+ (NSString*)oneMoonsMessage;
+ (NSString*)zeroMoonsMessage;
+ (NSString*)outOfGasMessage;

+ (NSArray*)wormHoleColors;
+ (NSArray*)planetColors;

+ (UIColor*)spcyBlueColor;
+ (UIColor*)spcyYellowColor;
+ (UIColor*)spcyPinkColor;
+ (UIColor*)spcyGreenColor;
+ (UIColor*)spcyWhiteColor;
+ (UIColor*)spcyGrayColor;
+ (UIColor*)spcyDisabledColor;
+ (UIColor*)spcyDisabledOverlayColor;

+ (UIColor*)colorFromHexString:(NSString*)hexString;

+ (float)lifeNodeWidth;

+ (BOOL)spcyDebugToggle;

@end
