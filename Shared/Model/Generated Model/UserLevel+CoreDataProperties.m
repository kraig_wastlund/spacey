//
//  UserLevel+CoreDataProperties.m
//  spacey
//
//  Created by Kraig Wastlund on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserLevel+CoreDataProperties.h"

@implementation UserLevel (CoreDataProperties)

@dynamic bestMoves;
@dynamic bestTime;
@dynamic levelNumber;

@end
