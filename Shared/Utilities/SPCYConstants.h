//
//  SPCYConstants.h
//  spacey
//
//  Created by Kraig Wastlund on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kCellTypeEmpty,
    kCellTypeSwitch,
    kCellTypePrice,
    kCellTypeCheck,
} SPCYCellType;

// sound strings
static NSString* const soundStringDeath = @"lose.wav";
static NSString* const soundStringRev = @"rev.wav";
static NSString* const soundStringIdle = @"idle.wav";
static NSString* const soundStringWin = @"win.wav";
static NSString* const soundStringClick = @"click.wav";

