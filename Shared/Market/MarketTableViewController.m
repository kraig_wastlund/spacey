//
//  MarketTableViewController.m
//  spacey
//
//  Created by Kraig Wastlund on 5/13/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "MarketTableViewController.h"
#import "Player.h"
#import "Level.h"
#import "AboutViewController.h"
#import "SPCYSettings.h"
#import "SPCYAlertView.h"
#import "User.h"
#import "SPCYSettings.h"
#import "SPCYButton.h"
#import "SPCYFormObject.h"
#import "SPCYCell.h"
@import StoreKit;
#import "KoldProducts.h"

static const int sectionHeight = 46;
static const int horizontalPadding = 30;
static const int tableViewHeaderHeight = 50;

@interface MarketTableViewController() <SPCYAlertDelegate>

@property (nonatomic, strong) NSArray* dataSource;
@property (nonatomic, strong) NSArray* sectionTitles;
@property (nonatomic, strong) NSDictionary* values;
@property (nonatomic, strong) User* currentUser;
@property (nonatomic, strong) NSArray<SKProduct*>* products;
@property (nonatomic, strong) KoldProducts* kold;
@property (nonatomic, strong) SPCYAlertView* alertView;

@end

@implementation MarketTableViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAPHelper.IAPHelperPurchaseNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.kold = [[KoldProducts alloc] init];
    
    self.currentUser = [User getUserInContext:self.context];
    
    // self.refreshControl = [[UIRefreshControl alloc] init];
    // [self.refreshControl addTarget:self action:@selector(reloadPurchases) forControlEvents:UIControlEventValueChanged];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self setupDataSource];
    
    // set nav bar to be clear
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    // buttons
    // back button
    SPCYButton* backButton = [SPCYButton buttonWithFrame:CGRectMake(0, 0, 50, 30) overlayImage:[UIImage imageNamed:@"quit_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
    [backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]]];
    
    [self.tableView registerClass:[SPCYCell class] forCellReuseIdentifier:@"cell"];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePurchaseNotification:) name:IAPHelper.IAPHelperPurchaseNotification object:nil];
    
    [self setupTableViewHeader];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadPurchases];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // remove possible alerts
    for (UIView* view in self.view.subviews)
    {
        if (view.tag == 99)
        {
            [view removeFromSuperview];
        }
    }
    
    [self.tableView reloadData];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


#pragma mark - TableView

- (void)setupTableViewHeader
{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, tableViewHeaderHeight)];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPadding, 0, self.tableView.frame.size.width - (horizontalPadding * 2), tableViewHeaderHeight - 1)];
    titleLabel.text = @"Upgrades";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:32 weight:UIFontWeightThin];
    [headerView addSubview:titleLabel];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(horizontalPadding, titleLabel.frame.size.height, self.tableView.frame.size.width - (horizontalPadding * 2), 1.0)];
    [line setBackgroundColor:[SPCYSettings spcyBlueColor]];
    [headerView addSubview:line];
    
    self.tableView.tableHeaderView = headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.dataSource objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SPCYCell calculatedCellHeight];
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    int titleHeight = 20;
    
    NSString* sectionTitle = [self.sectionTitles objectAtIndex:section];
    
    UIView* sectionView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.tableView.frame.size.width, sectionHeight)];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPadding, sectionHeight - titleHeight, sectionView.frame.size.width - (horizontalPadding * 2), titleHeight)];
    [titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightUltraLight]];
    [titleLabel setTextColor:[SPCYSettings spcyBlueColor]];
    titleLabel.text = sectionTitle;
    
    [sectionView addSubview:titleLabel];
    
    return sectionView;
}

- (SPCYCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    SPCYCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[SPCYCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(SPCYCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SPCYFormObject* formObject = [[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if ([formObject.title isEqualToString:@"restore"])
    {
        [self configureButtonCell:cell];
        
        return;
    }
    
    cell.titleLabel.text = formObject.title;
    cell.subtitleLabel.text = formObject.subTitle;
    if (formObject.rightSideNormal)
    {
        for (UIView* view in cell.rightComponent.subviews)
        {
            [view removeFromSuperview];
        }
        [cell.rightComponent addSubview: formObject.rightSideNormal];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rightSideNormal]|" options:0 metrics:nil views:@{ @"rightSideNormal": formObject.rightSideNormal }]];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightSideNormal]|" options:0 metrics:nil views:@{ @"rightSideNormal": formObject.rightSideNormal }]];
        
        [cell.rightComponent addSubview: formObject.rightSideSelected];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rightSideSelected]|" options:0 metrics:nil views:@{ @"rightSideSelected": formObject.rightSideSelected }]];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightSideSelected]|" options:0 metrics:nil views:@{ @"rightSideSelected": formObject.rightSideSelected }]];
        
        if (formObject.selected)
        {
            formObject.rightSideNormal.alpha = 0.0;
            formObject.rightSideSelected.alpha = 1.0;
        }
        else
        {
            formObject.rightSideNormal.alpha = 1.0;
            formObject.rightSideSelected.alpha = 0.0;
        }
    }
}

- (void)configureButtonCell:(SPCYCell*)cell
{
    UIButton* restoreButton = [[UIButton alloc] initWithFrame:CGRectZero];
    restoreButton.translatesAutoresizingMaskIntoConstraints = NO;
    [restoreButton setTitle:@"RESTORE PURCHASES" forState:UIControlStateNormal];
    [restoreButton setTitleColor:[SPCYSettings spcyBlueColor] forState:UIControlStateNormal];
    restoreButton.titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    restoreButton.layer.borderColor = [SPCYSettings spcyBlueColor].CGColor;
    restoreButton.layer.borderWidth = 1.0;
    restoreButton.layer.cornerRadius = 18.0;
    restoreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [restoreButton addTarget:self action:@selector(restoreButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.contentView addSubview:restoreButton];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[restore(200)]" options:0 metrics:nil views:@{ @"restore": restoreButton }]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[restore(36)]" options:0 metrics:nil views:@{ @"restore": restoreButton }]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:restoreButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:restoreButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:cell.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.products.count == 0)
    {
        [self triggerOfflineAlert];
        return;
    }
    SPCYFormObject* formObject = [[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (formObject.selected == YES)
    {
        return;
    }
    [self triggerAlertWithFormObject:formObject];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)triggerOfflineAlert
{
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithTitle:@"Offline" query:nil andMessage:@"I haven't been able to access the internet yet, please try again later"];
    alertView.name = @"offline";
    alertView.alpha = 0.0;
    alertView.tag = 99;
    alertView.alertDelegate = self;
    [self.view addSubview:alertView];
    [UIView animateWithDuration:0.5
                     animations:^{alertView.alpha = 1.0;}
                     completion:^(BOOL finished){
                         // do stuff here
                     }];
}

- (void)triggerAlertWithFormObject:(SPCYFormObject*)formObject
{
    NSString* title = [self alertTitleForFormObject:formObject];
    NSString* message = [self alertMessageForFormObject:formObject];
    
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithTitle:title query:@"Are you sure?" andMessage:message];
    alertView.alertDelegate = self;
    alertView.name = formObject.title;
    alertView.alpha = 0.0;
    alertView.tag = 99;
    [self.parentViewController.view addSubview:alertView];
    [UIView animateWithDuration:0.5
                     animations:^{alertView.alpha = 1.0;}
                     completion:^(BOOL finished){
                     }];
    self.alertView = alertView;
}

- (NSString*)alertTitleForFormObject:(SPCYFormObject*)formObject
{
    return [NSString stringWithFormat:@"Purchase %@?", formObject.title];
}

- (NSString*)alertMessageForFormObject:(SPCYFormObject*)formObject
{
    NSString* retString;
    
    if ([formObject.productIdentifier isEqualToString:spaceyIAP5Buckets()])
    {
        retString = @"Upgrade to having 5 lives. Gives you more time to solve puzzles between regeneration.\n\n$3.99 Price Upgrade.";
    }
    else if ([formObject.productIdentifier isEqualToString:spaceyIAP7Buckets()])
    {
        retString = @"Upgrade to having 7 lives. Gives you more time to solve puzzles between regeneration.\n\n$5.99 Price Upgrade.";
    }
    else if ([formObject.productIdentifier isEqualToString:spaceyIAP7BucketsAfter5()])
    {
        retString = @"Upgrade to having 7 lives. Gives you more time to solve puzzles between regeneration.\n\n$1.99 Price Upgrade.";
    }
    else if ([formObject.productIdentifier isEqualToString:spaceyIAP2xRegen()])
    {
        retString = @"Upgrade to regenerate lives twice as fast as normal.\n\n$3.99 Price Upgrade.";
    }
    else if ([formObject.productIdentifier isEqualToString:spaceyIAP3xRegen()])
    {
        retString = @"Upgrade to regenerate lives three times as fast as normal.\n\n$5.99 Price Upgrade.";
    }
    else if ([formObject.productIdentifier isEqualToString:spaceyIAP3xRegenAfter2()])
    {
        retString = @"Upgrade to regenerate lives three times as fast as normal.\n\n$1.99 Price Upgrade.";
    }

    return retString;
}

- (void)animateSelectionOfApplicableFormObjects
{
    NSMutableArray* animatingObjects = [[NSMutableArray alloc] init];
    
    for (int section = 0; section < [self.dataSource count]; section++)
    {
        for (SPCYFormObject* object in [self.dataSource objectAtIndex:section])
        {
            if (object.selected != (object.rightSideSelected.alpha == 1.0))
            {
                [animatingObjects addObject:object];
            }
        }
    }
    
    if ([animatingObjects count] > 0)
    {
        [UIView animateWithDuration:0.25 animations:^{
            
            for (SPCYFormObject* formObject in animatingObjects)
            {
                if (formObject.selected)
                {
                    formObject.rightSideNormal.alpha = 0.0;
                }
                else
                {
                    formObject.rightSideSelected.alpha = 0.0;
                }
            }
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.25 animations:^{
                
                for (SPCYFormObject* formObject in animatingObjects)
                {
                    if (formObject.selected)
                    {
                        formObject.rightSideSelected.alpha = 1.0;
                    }
                    else
                    {
                        formObject.rightSideNormal.alpha = 1.0;
                    }
                }
            }];
        }];
    }
}


#pragma mark - Data Source

- (UIView*)priceViewWithText:(NSString*)text
{
    UIView* priceView = [[UIView alloc] initWithFrame:CGRectZero];
    
    priceView.translatesAutoresizingMaskIntoConstraints = NO;
    UILabel* priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    priceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    priceLabel.text = text;
    priceLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    priceLabel.textColor = [SPCYSettings spcyGreenColor];
    priceLabel.layer.borderColor = [SPCYSettings spcyGreenColor].CGColor;
    priceLabel.layer.borderWidth = 1.0;
    priceLabel.layer.cornerRadius = 18.0;
    priceLabel.textAlignment = NSTextAlignmentCenter;
    [priceView addSubview:priceLabel];
    [priceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[priceLabel]|" options:0 metrics:nil views:@{ @"priceLabel": priceLabel }]];
    [priceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-9-[priceLabel]-9-|" options:0 metrics:nil views:@{ @"priceLabel": priceLabel }]];
    
    return priceView;
}

- (UIView*)selectedViewWithText:(NSString*)text
{
    UIView* selectedView = [[UIView alloc] initWithFrame:CGRectZero];
    
    selectedView.translatesAutoresizingMaskIntoConstraints = NO;
    UILabel* selectedLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    selectedLabel.translatesAutoresizingMaskIntoConstraints = NO;
    selectedLabel.text = text;
    selectedLabel.font = [UIFont systemFontOfSize:11 weight:UIFontWeightRegular];
    selectedLabel.textColor = [SPCYSettings spcyGreenColor];
    selectedLabel.textAlignment = NSTextAlignmentCenter;
    [selectedView addSubview:selectedLabel];
    [selectedView addConstraint:[NSLayoutConstraint constraintWithItem:selectedLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:selectedView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [selectedView addConstraint:[NSLayoutConstraint constraintWithItem:selectedLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:selectedView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    return selectedView;
}

- (void)setupDataSource
{
    User* currentUser = [User getUserInContext:self.context];
    SPCYFormObject* fiveLives = [[SPCYFormObject alloc] init];
    fiveLives.selected = currentUser.maxLives.integerValue >= 5;
    fiveLives.title = @"5 Buckets";
    fiveLives.subTitle = @"3 can't be enough, so here's 5.";
    fiveLives.rightSideSelected = [self selectedViewWithText:@"PURCHASED"];
    fiveLives.rightSideNormal = [self priceViewWithText:@"$3.99"];
    fiveLives.productIdentifier = spaceyIAP5Buckets();
    
    SPCYFormObject* sevenLives = [[SPCYFormObject alloc] init];
    sevenLives.selected = currentUser.maxLives.integerValue >= 7;
    sevenLives.title = @"7 Buckets";
    sevenLives.subTitle = @"More failure leads to more success. Right?";
    sevenLives.rightSideSelected = [self selectedViewWithText:@"PURCHASED"];
    
    if (fiveLives.selected) {
        sevenLives.rightSideNormal = [self priceViewWithText:@"$1.99"];
        sevenLives.productIdentifier = spaceyIAP7BucketsAfter5();
    } else {
        sevenLives.rightSideNormal = [self priceViewWithText:@"$5.99"];
        sevenLives.productIdentifier = spaceyIAP7Buckets();
    }
    
    SPCYFormObject* twoRegen = [[SPCYFormObject alloc] init];
    twoRegen.selected = currentUser.livesModifier.integerValue >= 2;
    twoRegen.title = @"Blazing";
    twoRegen.subTitle = @"You need this, I know. You're welcome.";
    twoRegen.rightSideSelected = [self selectedViewWithText:@"PURCHASED"];
    twoRegen.rightSideNormal = [self priceViewWithText:@"$3.99"];
    twoRegen.productIdentifier = spaceyIAP2xRegen();
    
    SPCYFormObject* threeRegen = [[SPCYFormObject alloc] init];
    threeRegen.selected = currentUser.livesModifier.integerValue >= 3;
    threeRegen.title = @"Light Speed";
    threeRegen.subTitle = @"No patience? Here you go.";
    threeRegen.rightSideSelected = [self selectedViewWithText:@"PURCHASED"];
    
    if (twoRegen.selected) {
        threeRegen.rightSideNormal = [self priceViewWithText:@"$1.99"];
        threeRegen.productIdentifier = spaceyIAP3xRegenAfter2();
    } else {
        threeRegen.rightSideNormal = [self priceViewWithText:@"$5.99"];
        threeRegen.productIdentifier = spaceyIAP3xRegen();
    }
    
    SPCYFormObject* button = [[SPCYFormObject alloc] init];
    button.title = @"restore";
    
    self.dataSource = @[
                        @[fiveLives, sevenLives],
                        @[twoRegen, threeRegen],
                        @[button]
                        ];
    self.sectionTitles = @[@"PERMANENT LIFE BUCKETS", @"LIFE REGENERATION SPEED", @""];
}


#pragma mark - actions

- (void)backButtonPressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)restoreButtonPressed
{
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithTitle:@"Restore Purchases" query:@"Are you sure?" andMessage:@"Would you like to restore upgrades that you have already purchased?"];
    alertView.alertDelegate = self;
    alertView.name = @"restore";
    alertView.alpha = 0.0;
    alertView.tag = 99;
    [self.view addSubview:alertView];
    [UIView animateWithDuration:0.5
                     animations:^{alertView.alpha = 1.0;}
                     completion:^(BOOL finished){
                         // do stuff here
                     }];
}


#pragma mark - Form Object Helper

- (SPCYFormObject*)formObjectWithName:(NSString*)name
{
    for (int section = 0; section < [self.dataSource count]; section++)
    {
        for (SPCYFormObject* object in [self.dataSource objectAtIndex:section])
        {
            if ([object.title isEqualToString:name])
            {
                return object;
            }
        }
    }
    
    return nil;
}

- (SPCYFormObject*)getObjectWithProductIdentifier:(NSString*)prodId
{
    for (int section = 0; section < [self.dataSource count]; section++)
    {
        for (SPCYFormObject* object in [self.dataSource objectAtIndex:section])
        {
            if ([object.productIdentifier isEqualToString:prodId])
            {
                return object;
            }
        }
    }
    
    return nil;
}

#pragma mark - AlertView Delegate

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType
{
    if (buttonType == kTypeOk && ![view.name isEqualToString:@"restore"] && ![view.name isEqualToString:@"offline"])
    {
        SKProduct* product = [self productForFormObject:[self formObjectWithName:view.name]];
        [[KoldProducts store] buyProduct:product];
    }
    else if (buttonType == kTypeOk && [view.name isEqualToString:@"restore"])
    {
        [KoldProducts.store restorePurchases];
    }
    
    // dismiss
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

- (void)handleAlertView
{
    if (self.alertView)
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.alertView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.alertView removeFromSuperview];
            [UIView animateWithDuration:0.5 animations:^{self.alertView.alpha = 0.0;} completion:^(BOOL finished) {
                self.alertView = nil;
            }];
        }];
    }
}

- (SKProduct*)productForFormObject:(SPCYFormObject*)object
{
    assert(self.products.count == 6);
    SKProduct* product;
    
    for (SKProduct* thisProd in self.products)
    {
        if ([thisProd.productIdentifier isEqualToString:object.productIdentifier])
        {
            product = thisProd;
        }
    }
    
    assert(product != nil);
    
    return product;
}


#pragma mark - Store IAP

- (void)reloadPurchases
{
    if (self.products == nil)
    {
        self.products = [[NSMutableArray alloc] init];
    }
    [self.tableView reloadData];
    
    [KoldProducts.store requestProducts:^(BOOL success, NSArray<SKProduct *> * _Nullable products) {
        if (success == YES)
        {
            self.products = products;
            [self.tableView reloadData];
        }
        
        [self.refreshControl endRefreshing];
    }];
}

- (void)handlePurchaseNotification:(NSNotification*)notification
{
    NSString* productId = notification.object;
    assert(productId != nil);
    
    for (id product in self.products) {
        NSLog(@"%@", product);
    }
    
    BOOL needUIRefresh = NO;
    for (NSArray* section in self.dataSource)
    {
        for (SPCYFormObject* object in section)
        {
            if ([object.productIdentifier isEqualToString:productId] && object.selected == NO)
            {
                needUIRefresh = YES;
                object.selected = YES;
            }
        }
    }
    
    if (needUIRefresh)
    {
        [UIView animateWithDuration:0.5 animations:^{
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
            } completion:^(BOOL finished) {
                [self animateSelectionOfApplicableFormObjects];
            }];
        }];
        [self updateUserWithPurchasedProductId:productId];
    }
    
    [self handleAlertView];
}


#pragma mark - Model Update

- (void)updateUserWithPurchasedProductId:(NSString*)productId
{
    User* currentUser = [User getUserInContext:self.context];
    if ([productId isEqualToString:spaceyIAP7Buckets()])
    {
        [currentUser setMaxLives:@7];
    }
    else if ([productId isEqualToString:spaceyIAP7BucketsAfter5()])
    {
        [currentUser setMaxLives:@7];
    }
    else if ([productId isEqualToString:spaceyIAP5Buckets()] && currentUser.maxLives.integerValue < 5)
    {
        [currentUser setMaxLives:@5];
        // get the object and update it
        SPCYFormObject* object = [self getObjectWithProductIdentifier:spaceyIAP7Buckets()];
        object.rightSideNormal = [self priceViewWithText:@"$1.99"];
        object.productIdentifier = spaceyIAP7BucketsAfter5();
        [self.tableView reloadData];
    }
    else if ([productId isEqualToString:spaceyIAP3xRegen()])
    {
        [currentUser setLivesModifier:@3];
    }
    else if ([productId isEqualToString:spaceyIAP3xRegenAfter2()])
    {
        [currentUser setLivesModifier:@3];
    }
    else if ([productId isEqualToString:spaceyIAP2xRegen()] && currentUser.livesModifier.integerValue < 2)
    {
        [currentUser setLivesModifier:@2];
        // get the object and update it
        SPCYFormObject* object = [self getObjectWithProductIdentifier:spaceyIAP3xRegen()];
        object.rightSideNormal = [self priceViewWithText:@"$1.99"];
        object.productIdentifier = spaceyIAP3xRegenAfter2();
        [self.tableView reloadData];
    }
    else
    {
        assert(NO);
    }
}

@end
