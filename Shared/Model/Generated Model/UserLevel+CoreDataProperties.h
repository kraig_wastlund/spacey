//
//  UserLevel+CoreDataProperties.h
//  spacey
//
//  Created by Kraig Wastlund on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserLevel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserLevel (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *bestMoves;
@property (nullable, nonatomic, retain) NSNumber *bestTime;
@property (nullable, nonatomic, retain) NSNumber *levelNumber;

@end

NS_ASSUME_NONNULL_END
