//
//  Node.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#define ARC4RANDOM_MAX      0x100000000

#import "Node.h"
#import "Level.h"

@implementation Node

+ (float)randFloatBetween:(float)low and:(float)high
{
    float diff = high - low;
    return (((float) rand() / RAND_MAX) * diff) + low;
}

+ (NSString*)nameForType:(NodeType)type
{
    if (type == kStart)
    {
        return @"start";
    }
    if (type == kEnd)
    {
        return @"end";
    }
    if (type == kBlackHole)
    {
        return @"blackhole";
    }
    
    return @"middle";
}

+ (Node*)setupNodeWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level
{
    Node* newNode = [[Node alloc] init];
    newNode.type = [nodeDict objectForKey:@"type"];
    newNode.x = [nodeDict objectForKey:@"x"];
    newNode.y = [nodeDict objectForKey:@"y"];
    
    NSMutableArray* arrayOfTextures = [[NSMutableArray alloc] init];
    if (newNode.type.integerValue == kStart || newNode.type.integerValue == kMiddle)
    {
        int randomIndex = arc4random() % 4 + 1;
        [newNode setTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"asteroid%d", randomIndex]]];
    }
    else if (newNode.type.integerValue == kEnd)
    {
        for (int i = 0; i <= 119; i++)
        {
            NSString* name = @"end";
            NSString* fileName = [Node fileNameForName:name andIndex:i];
            [arrayOfTextures addObject:[SKTexture textureWithImageNamed:fileName]];
        }
    }
    
    if ([arrayOfTextures count] > 0)
    {
        [newNode runAction:[SKAction repeatActionForever:[SKAction animateWithTextures:arrayOfTextures timePerFrame:0.125]]];
    }
    
    if (newNode.type.integerValue == kEnd)
    {
        newNode.size = CGSizeMake(level.cellSize.width * 1.15, level.cellSize.height * 1.15);
    }
    else // kMiddle or kStart
    {
        float percent = [Node randFloatBetween:0.50 and:0.95];
        float nodeWidth = level.cellSize.width * percent;
        float nodeHeight = level.cellSize.height * percent;
        newNode.size = CGSizeMake(nodeWidth, nodeHeight);
        
        double randomRotation = ((double)arc4random() / ARC4RANDOM_MAX);
        randomRotation = arc4random() % 2 == 0 ? -randomRotation : randomRotation;
        float randomSpeed = ((float)rand() / RAND_MAX) * 2 + 0.5; // random float between 0.5 and 2.5
        SKAction* rotateAction = [SKAction rotateByAngle:randomRotation duration:randomSpeed];
        [newNode runAction:[SKAction repeatActionForever:rotateAction]];
    }
    
    CGFloat deltaY = [[UIScreen mainScreen] bounds].size.height * 0.16;
    CGFloat deltaX = 6;
    newNode.position = CGPointMake(.5 * level.cellSize.width + newNode.x.doubleValue * level.cellSize.width + deltaX, .5 * level.cellSize.height + newNode.y.doubleValue * level.cellSize.height + deltaY);
    
    return newNode;
}

+ (SKShapeNode*)planetCircleBehindNodeWithEnd:(Node*)end
{
    CGSize circleSize = CGSizeMake(end.size.width * 0.9, end.size.height * 0.9);
    CGRect circle = CGRectMake(0, 0, circleSize.width, circleSize.height);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.fillColor = [SKColor colorWithRed:255.0/255.0 green:229.0/255.0 blue:0.0/255.5 alpha:1.0];
    shapeNode.lineWidth = 0;
    
    return shapeNode;
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
