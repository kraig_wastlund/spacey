//
//  UserLevel.m
//  spacey
//
//  Created by Donald Timpson on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "UserLevel.h"

@implementation UserLevel

+ (UserLevel*)createUserLevelWithLevelNumber:(NSNumber*)levelNumber inContext:(NSManagedObjectContext*)context
{
    UserLevel* userLevel = [NSEntityDescription insertNewObjectForEntityForName:@"UserLevel" inManagedObjectContext:context];
    userLevel.levelNumber = levelNumber;
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return userLevel;
}

+ (UserLevel*)getUserLevelWithLevelNumber:(NSNumber*)levelNumber InContext:(NSManagedObjectContext*)context
{
    NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:@"UserLevel"];
    request.predicate = [NSPredicate predicateWithFormat:@"levelNumber == %@", levelNumber];
    UserLevel* userlevel = [[context executeFetchRequest:request error:nil] firstObject];
    
    if (userlevel == nil)
    {
        userlevel = [UserLevel createUserLevelWithLevelNumber:levelNumber inContext:context];
    }
    
    return userlevel;
}

- (void)setBestTime:(NSNumber *)bestTime
{
    if (self.bestTime != nil && bestTime.doubleValue >= self.bestTime.doubleValue)
    {
        return;
    }
    
    [self willChangeValueForKey:@"bestTime"];
    [self setPrimitiveValue:bestTime forKey:@"bestTime"];
    [self didChangeValueForKey:@"bestTime"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

- (void)setBestMoves:(NSNumber *)bestMoves
{
    if (self.bestMoves != nil && bestMoves.integerValue >= self.bestMoves.integerValue)
    {
        return;
    }
    
    [self willChangeValueForKey:@"bestMoves"];
    [self setPrimitiveValue:bestMoves forKey:@"bestMoves"];
    [self didChangeValueForKey:@"bestMoves"];
    
    NSError* error;
    [self.managedObjectContext save:&error];
    assert(error == nil);
}

@end
