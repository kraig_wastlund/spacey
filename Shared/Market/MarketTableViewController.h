//
//  MarketTableViewController.h
//  spacey
//
//  Created by Kraig Wastlund on 5/13/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketTableViewController : UITableViewController

@property (nonatomic, strong) NSManagedObjectContext* context;

@end
