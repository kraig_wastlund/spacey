//
//  WormHole.m
//  code_camp_2015
//
//  Created by Donald Timpson on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "WormHole.h"
#import "Level.h"
#import "SPCYSettings.h"

@implementation WormHole

static double const marginPercent = .05;

+ (WormHole*)setupWormHoleWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level
{
    WormHole* wormHole = [[WormHole alloc] init];
    wormHole.type = [nodeDict objectForKey:@"type"];
    wormHole.x = [nodeDict objectForKey:@"x"];
    wormHole.y = [nodeDict objectForKey:@"y"];
    wormHole.name = @"wormhole";
    wormHole.linkId = [nodeDict objectForKey:@"linkId"];
    
    [wormHole setTexture:[SKTexture textureWithImageNamed:@"wormhole"]];
    SKAction *oneRevolution = [SKAction rotateByAngle:-M_PI*2 duration: 2.25];
    SKAction *repeat = [SKAction repeatActionForever:oneRevolution];
    [wormHole runAction:repeat];
    
    CGFloat gridSquareWidth = level.width.doubleValue / level.numberOfColumns.doubleValue;
    CGFloat gridSquareHeight = level.height.doubleValue / level.numberOfRows.doubleValue;
    
    float width = gridSquareWidth - (2 * (marginPercent * gridSquareWidth));
    float height = gridSquareHeight - (2 * (marginPercent * gridSquareHeight));
    [wormHole setSize:CGSizeMake(width * 0.75, height * 0.75)];
    CGFloat deltaY = [[UIScreen mainScreen] bounds].size.height * 0.16;
    CGFloat deltaX = 6;
    wormHole.position = CGPointMake(.5 * gridSquareWidth + wormHole.x.doubleValue * gridSquareWidth + deltaX, .5 * gridSquareHeight + wormHole.y.doubleValue * gridSquareHeight + deltaY);
    
    // inner graphic
    WormHole* innerWorm = [[WormHole alloc] init];
    innerWorm.linkId = wormHole.linkId;
    [innerWorm setTexture:[SKTexture textureWithImageNamed:@"wormhole"]];
    [innerWorm setSize:CGSizeMake(wormHole.size.width * 0.75, wormHole.size.height * 0.75)];
    [innerWorm runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:M_PI*2 duration: 1.25]]];
    SKAction* sequence = [SKAction sequence:@[
                                              [SKAction scaleTo:0.6 duration:0.15],
                                              [SKAction scaleTo:1.0 duration:0.15],
                                              [SKAction waitForDuration:0.5],
                                              ]];
    [innerWorm runAction:[SKAction repeatActionForever:sequence]];
    [wormHole addChild:innerWorm];
    [innerWorm setZPosition:10];
    
    [innerWorm drawPlanetBehind];
    
    [wormHole drawPlanetBehind];
    
    return wormHole;
}

- (void)drawPlanetBehind
{
    CGSize circleSize = self.size;
    CGRect circle = CGRectMake(0, 0, circleSize.width, circleSize.height);
    SKShapeNode *shapeNode = [[SKShapeNode alloc] init];
    shapeNode.path = [UIBezierPath bezierPathWithOvalInRect:circle].CGPath;
    shapeNode.lineWidth = 0;
    shapeNode.name = @"wormhole_background";
    NSInteger colorIndex = [self linkId].integerValue - 1;
    shapeNode.fillColor = [[SPCYSettings wormHoleColors] objectAtIndex:colorIndex % [[SPCYSettings wormHoleColors] count]];
//    shapeNode.fillColor = [SPCYSettings colorFromHexString:@"7FFF00"];
    [shapeNode setPosition:CGPointMake(0 - self.size.width * 0.5, 0 - self.size.height * 0.5)];
    [self addChild:shapeNode];
}

+ (NSString*)fileNameForName:(NSString*)name andIndex:(int)i
{
    NSString* imageName = [[NSString alloc] init];
    
    if (i < 10)
    {
        imageName = [NSString stringWithFormat:@"%@_00%d", name, i];
    }
    else if (i < 100)
    {
        imageName = [NSString stringWithFormat:@"%@_0%d", name, i];
    }
    else
    {
        imageName = [NSString stringWithFormat:@"%@_%d", name, i];
    }
    
    return imageName;
}

@end
