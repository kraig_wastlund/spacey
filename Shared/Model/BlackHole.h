//
//  BlackHole.h
//  spacey
//
//  Created by Donald Timpson on 11/29/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "Node.h"

@interface BlackHole : Node

+ (BlackHole*)setupBlackHoleWithDictionary:(NSDictionary*)nodeDict forLevel:(Level*)level;

@end
