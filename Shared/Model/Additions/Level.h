//
//  Level+Additions.h
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Node.h"

@interface Level : NSObject

@property (strong, nonatomic) Node* start;
@property (strong, nonatomic) Node* end;
@property (strong, nonatomic) NSArray* nodes;
@property (strong, nonatomic) NSArray* wormHoles;
@property (strong, nonatomic) NSArray* blackHoles;
@property (strong, nonatomic) NSArray* middleNodes;
@property (strong, nonatomic) NSNumber* numberOfRows;
@property (strong, nonatomic) NSNumber* numberOfColumns;
@property (strong, nonatomic) NSNumber* width;
@property (strong, nonatomic) NSNumber* height;
@property (strong, nonatomic) NSNumber* moves;
@property (nonatomic, strong) NSNumber* distance;
@property (nonatomic, strong) NSNumber* levelNumber;
@property CGSize cellSize;

+ (Level*)getLevelWithName:(NSNumber*)levelNumber width:(NSNumber*)width height:(NSNumber*)height;

+ (NSNumber*)getScoreForLevel:(Level*)level withTime:(NSNumber*)time moves:(NSNumber*)moves fps:(NSNumber*)fps andPlayerSpeed:(NSNumber*)playerSpeed;

+ (NSInteger)getTotalNumberOfLevels;

- (NSInteger)getNumberOfMoves;

- (NSInteger)getTimeForPlayerSpeed:(NSInteger)playerSpeed andFPS:(CGFloat)fps;

@end