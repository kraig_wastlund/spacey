//
//  SPCYAlertView.m
//  spacey
//
//  Created by Kraig Wastlund on 11/21/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#define isiPhone4  ([[UIScreen mainScreen] bounds].size.height == 480)?TRUE:FALSE

#import "SPCYAlertView.h"
#import "Level.h"
#import "SPCYSettings.h"
#import "SPCYButton.h"
#import "UserLevel.h"

@implementation SPCYAlertView

- (instancetype)initWithTitle:(NSString*)title andQuery:(NSString*)query
{
    self = [super init];
    
    if (self)
    {
        int screenWidth = [UIScreen mainScreen].bounds.size.width;
        int screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        [self setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
        
        int width = 260; // width of alert view
        int labelWidth = width - 20;
        
        int titleHeight = 40;
        int confirmationHeight = 70;
        int buttonPadding = width * 0.02;
        int buttonHeight = 50;
        int paddingBetweenConfirmationAndButtons = buttonPadding;
        int buttonHeightWithPadding = buttonHeight + paddingBetweenConfirmationAndButtons + buttonPadding;
        
        int height = titleHeight + confirmationHeight + buttonHeightWithPadding;
        
        // View Frame
        int y = screenHeight * 0.5 - (height * 0.5);
        int x = screenWidth * 0.5 - (width * 0.5);
        CGRect viewFrame = CGRectMake(x, y, width, height);
        // this uiview is where everything lives, it gets put onto self (this was just in case i wanted it to be slightly smaller than the overall view with a different background color)
        UIView* alertView = [[UIView alloc] initWithFrame:viewFrame];
        alertView.layer.cornerRadius = viewFrame.size.height * 0.05;
        alertView.layer.masksToBounds = YES;
        [alertView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:1.0]];
        
        // incrementing Y
        int incrementingY = 0;
        
        // title
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementingY, labelWidth, titleHeight)];
        [titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:[NSString stringWithFormat:@"%@", title]];
        
        // separator
        UIView* separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, titleLabel.frame.size.height - 1, labelWidth, 0.5)];
        [separatorView setBackgroundColor:[UIColor whiteColor]];
        [titleLabel addSubview:separatorView];
        
        [alertView addSubview:titleLabel];
        
        incrementingY += titleLabel.frame.size.height;
        
        // are you sure?
        UILabel* confirmationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementingY, labelWidth, confirmationHeight)];
        [confirmationLabel setFont:[UIFont systemFontOfSize:28 weight:UIFontWeightUltraLight]];
        [confirmationLabel setTextColor:[UIColor whiteColor]];
        [confirmationLabel setTextAlignment:NSTextAlignmentCenter];
        [confirmationLabel setNumberOfLines:0];
        [confirmationLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [confirmationLabel setText:[NSString stringWithFormat:query]];
        
        // separator
        UIView* separator3View = [[UIView alloc] initWithFrame:CGRectMake(0, confirmationHeight - 1, labelWidth, 0.5)];
        [separator3View setBackgroundColor:[UIColor whiteColor]];
        [confirmationLabel addSubview:separator3View];
        
        [alertView addSubview:confirmationLabel];
        
        // button 1 (left button)
        int buttonY = alertView.frame.size.height - buttonHeight - buttonPadding;
        int buttonWidth = (alertView.frame.size.width - (3 * buttonPadding)) / 2;
        CGRect buttonFrame = CGRectMake(buttonPadding, buttonY, buttonWidth, buttonHeight);
        SPCYButton* button1 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"cancel_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
        [button1 addTarget:self action:@selector(noButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button1];
        
        // button 3 (right button)
        int buttonX = buttonPadding * 2 + buttonWidth;
        buttonFrame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
        SPCYButton* button3 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"ok_overlay"] andBackgroundColor:[SPCYSettings spcyGreenColor]];
        [button3 addTarget:self action:@selector(yesButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button3];
        
        [self addSubview:alertView];
        [self bringSubviewToFront:alertView];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString*)title query:(NSString*)query andMessage:(NSString*)message // pass in nil query to get just the "Ok" button
{
    self = [super init];
    
    if (self)
    {
        int screenWidth = [UIScreen mainScreen].bounds.size.width;
        int screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        [self setFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
        
        int width = 260; // width of alert view
        int labelWidth = width - 20;
        
        // calculate the additional height and increment
        UILabel *gettingSizeLabel = [[UILabel alloc] init];
        gettingSizeLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight];
        gettingSizeLabel.text = message;
        gettingSizeLabel.numberOfLines = 0;
        gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        CGSize maximumLabelSize = CGSizeMake(labelWidth, CGFLOAT_MAX);
        CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
        
        int titleHeight = 40;
        int messageHeight = expectSize.height + 20;
        int confirmationHeight = 70;
        int buttonPadding = width * 0.02;
        int buttonHeight = 50;
        int paddingBetweenConfirmationAndButtons = buttonPadding;
        int buttonHeightWithPadding = buttonHeight + paddingBetweenConfirmationAndButtons + buttonPadding;
        
        int height = query.length > 0 ? titleHeight + messageHeight + confirmationHeight + buttonHeightWithPadding : titleHeight + messageHeight + buttonHeightWithPadding;
        
        // View Frame
        int y = screenHeight * 0.5 - (height * 0.5);
        int x = screenWidth * 0.5 - (width * 0.5);
        CGRect viewFrame = CGRectMake(x, y, width, height);
        // this uiview is where everything lives, it gets put onto self (this was just in case i wanted it to be slightly smaller than the overall view with a different background color)
        UIView* alertView = [[UIView alloc] initWithFrame:viewFrame];
        alertView.layer.cornerRadius = viewFrame.size.height * 0.05;
        alertView.layer.masksToBounds = YES;
        [alertView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:1.0]];
        
        // incrementing Y
        int incrementingY = 0;
        
        // title
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementingY, labelWidth, titleHeight)];
        [titleLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight]];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setText:[NSString stringWithFormat:@"%@", title]];
        
        // separator
        UIView* separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, titleLabel.frame.size.height - 1, labelWidth, 0.5)];
        [separatorView setBackgroundColor:[UIColor whiteColor]];
        [titleLabel addSubview:separatorView];
        
        [alertView addSubview:titleLabel];
        
        incrementingY += titleLabel.frame.size.height;
        
        // message
        UILabel* messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementingY, labelWidth, messageHeight)];
        [messageLabel setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightUltraLight]];
        [messageLabel setTextColor:[UIColor whiteColor]];
        [messageLabel setTextAlignment:NSTextAlignmentCenter];
        [messageLabel setNumberOfLines:0];
        [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [messageLabel setText:message];
        
        // separator
        UIView* separator2View = [[UIView alloc] initWithFrame:CGRectMake(0, messageHeight - 1, labelWidth, 0.5)];
        [separator2View setBackgroundColor:[UIColor whiteColor]];
        [messageLabel addSubview:separator2View];
        
        [alertView addSubview:messageLabel];
        
        incrementingY += messageLabel.frame.size.height;
        
        if (query.length > 0) // ok and cancel button with query
        {
            // are you sure?
            UILabel* confirmationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, incrementingY, labelWidth, confirmationHeight)];
            [confirmationLabel setFont:[UIFont systemFontOfSize:28 weight:UIFontWeightUltraLight]];
            [confirmationLabel setTextColor:[UIColor whiteColor]];
            [confirmationLabel setTextAlignment:NSTextAlignmentCenter];
            [confirmationLabel setNumberOfLines:0];
            [confirmationLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [confirmationLabel setText:[NSString stringWithFormat:query]];
            
            // separator
            UIView* separator3View = [[UIView alloc] initWithFrame:CGRectMake(0, confirmationHeight - 1, labelWidth, 0.5)];
            [separator3View setBackgroundColor:[UIColor whiteColor]];
            [confirmationLabel addSubview:separator3View];
            
            [alertView addSubview:confirmationLabel];
            
            // button 1 (left button)
            int buttonY = alertView.frame.size.height - buttonHeight - buttonPadding;
            int buttonWidth = (alertView.frame.size.width - (3 * buttonPadding)) / 2;
            CGRect buttonFrame = CGRectMake(buttonPadding, buttonY, buttonWidth, buttonHeight);
            SPCYButton* button1 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"cancel_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
            [button1 addTarget:self action:@selector(noButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [alertView addSubview:button1];
            
            // button 3 (right button)
            int buttonX = buttonPadding * 2 + buttonWidth;
            buttonFrame = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
            SPCYButton* button3 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"ok_overlay"] andBackgroundColor:[SPCYSettings spcyGreenColor]];
            [button3 addTarget:self action:@selector(yesButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [alertView addSubview:button3];
        }
        else // ok button only
        {
            // button 1 (ok button -- full width)
            int buttonY = alertView.frame.size.height - buttonHeight - buttonPadding;
            int buttonWidth = (alertView.frame.size.width - (2 * buttonPadding));
            CGRect buttonFrame = CGRectMake(buttonPadding, buttonY, buttonWidth, buttonHeight);
            SPCYButton* button1 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"ok_overlay"] andBackgroundColor:[SPCYSettings spcyGreenColor]];
            [button1 addTarget:self action:@selector(yesButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [alertView addSubview:button1];
        }
        
        [self addSubview:alertView];
        [self bringSubviewToFront:alertView];
    }
    
    return self;
}

- (instancetype)initTutorialAlert
{
    self = [super init];
    if (self)
    {
        [self setFrame:[UIScreen mainScreen].bounds];
        
        UIImageView* backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]];
        [backgroundImage setFrame:self.frame];
        [self addSubview:backgroundImage];
        
        UITapGestureRecognizer* gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTutorialAlert:)];
        [self addGestureRecognizer:gr];
        
        float shipWidth = self.frame.size.width * 0.35;
        float shipHeight = shipWidth * 0.75;
        float arrowsWidth = shipWidth * 1.5;
        float centerOfView = self.frame.size.width * 0.5;
        float centerOfShipY = self.frame.size.height * 0.25;
        UIImageView* shipImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"spaceyship"]];
        [shipImage setFrame:CGRectMake(centerOfView - shipWidth * 0.5, centerOfShipY - shipHeight * 0.5, shipWidth, shipHeight)];
        [self addSubview:shipImage];
        
        UIImageView* arrowsView = [[UIImageView alloc] initWithFrame:CGRectMake(centerOfView - arrowsWidth * 0.5, centerOfShipY - arrowsWidth * 0.5, arrowsWidth, arrowsWidth)];
        [arrowsView setImage:[UIImage imageNamed:@"tutorial_arrows"]];
        [self addSubview:arrowsView];
        [self bringSubviewToFront:shipImage];
        
        float nextY = centerOfShipY + arrowsWidth * 0.5;
        float screenWidth = self.frame.size.width;
        
        UILabel* tutorialMessage = [[UILabel alloc] initWithFrame:CGRectMake(30, nextY, screenWidth - 60, 200)];
        [tutorialMessage setText:@"Welcome to Spacey.\nShe can move in four directions.\n(Up, Down, Left, Right)"];
        [tutorialMessage setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightUltraLight]];
        [tutorialMessage setTextAlignment:NSTextAlignmentCenter];
        [tutorialMessage setTextColor:[UIColor whiteColor]];
        [tutorialMessage setLineBreakMode:NSLineBreakByWordWrapping];
        [tutorialMessage setNumberOfLines:0];
        [self addSubview:tutorialMessage];
        
        float nearBottomY = self.frame.size.height - 50;
        
        UILabel* tapToDismissMessage = [[UILabel alloc] initWithFrame:CGRectMake(30, nearBottomY, screenWidth - 60, 30)];
        [tapToDismissMessage setText:@"tap anywhere to dismiss"];
        [tapToDismissMessage setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightUltraLight]];
        [tapToDismissMessage setTextAlignment:NSTextAlignmentCenter];
        [tapToDismissMessage setTextColor:[UIColor whiteColor]];
        [self addSubview:tapToDismissMessage];
    }
    
    return self;
}

- (void)dismissTutorialAlert:(UITapGestureRecognizer*)gr
{
    [UIView animateWithDuration:0.5
                     animations:^{self.alpha = 0.0;}
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     [self.alertDelegate alertView:self didSelectWithIndex:kTypeTutorial];
                     }];
}

- (instancetype)initWithFrame:(CGRect)frame levelNumber:(NSNumber*)levelNumber currentTime:(NSNumber*)currentTime currentMoves:(NSNumber*)currentMoves numberOfLives:(NSNumber*)numberOfLives fps:(NSNumber*)fps playerSpeed:(NSNumber*)playerSpeed level:(Level*)level andGameCode:(FinishedGameCode)gameCode plusContext:(NSManagedObjectContext*)context
{
    UserLevel* thisLevel = [UserLevel getUserLevelWithLevelNumber:levelNumber InContext:context];
    float bestTime = thisLevel.bestTime.floatValue;
    // int bestMoves = thisLevel.bestMoves.integerValue;
    NSNumber* score = [Level getScoreForLevel:level withTime:currentTime moves:currentMoves fps:fps andPlayerSpeed:playerSpeed];
    
    self = [super init];
    
    if (self)
    {
        BOOL moreLevels = levelNumber.integerValue < [Level getTotalNumberOfLevels];
        
        // locations
        int x = 0;
        int y = [UIScreen mainScreen].bounds.size.height - frame.size.height;
        int width = frame.size.width;
        int height = frame.size.height;
        int center = width / 2;
        int verticalPadding = height * 0.02;
        
        [self setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        
        CGRect viewFrame = CGRectMake(x, y, width, height);
        // this uiview is where everything lives, it gets put onto self (this was just in case i wanted it to be slightly smaller than the overall view with a different background color)
        UIView* alertView = [[UIView alloc] initWithFrame:viewFrame];
        
        int elementY = verticalPadding; // this is where the y starts at the top
        
        if (isiPhone4)
        {
            verticalPadding = 0;
            elementY = -10;
        }
        
        // game message
        NSString* message;
        if (gameCode == kGameWon && moreLevels == NO)
        {
            message = [SPCYSettings gameCompletedMessage];
        }
        else if (gameCode == kGameQuit)
        {
            message = [SPCYSettings gameQuitMessage];
        }
        else if (gameCode == kGameLost)
        {
            message = [SPCYSettings gameLostMessage];
        }
        else
        {
            if (score.integerValue == 3)
            {
                message = [SPCYSettings threeMoonsMessage];
            }
            else if (score.integerValue == 2)
            {
                message = [SPCYSettings twoMoonsMessage];
            }
            else if (score.integerValue == 1)
            {
                message = [SPCYSettings oneMoonsMessage];
            }
            else
            {
                message = [SPCYSettings zeroMoonsMessage];
            }
        }
        
        UILabel* messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, elementY, alertView.frame.size.width - 30, 60)];
        [messageLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [messageLabel setTextColor:[UIColor whiteColor]];
        [messageLabel setTextAlignment:NSTextAlignmentCenter];
        [messageLabel setText:message];
        [messageLabel setNumberOfLines:0];
        [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [alertView addSubview:messageLabel];
        
        elementY += messageLabel.frame.size.height + verticalPadding;
        
        // level representation
        int imageWidth = width * 0.25;
        NSString* imageName;
        if (gameCode == kGameQuit)
        {
            imageName = @"spaceyship";
        }
        else if (gameCode == kGameLost)
        {
            imageName = @"spaceyship";
        }
        else
        {
            imageName = @"spaceyship";
        }
        UIImageView* levelImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        [levelImageView setFrame:CGRectMake(center - imageWidth / 2, elementY, imageWidth, imageWidth)];
        [levelImageView setContentMode:UIViewContentModeScaleAspectFill];
        [alertView addSubview:levelImageView];
        
        elementY += levelImageView.frame.size.height + verticalPadding;
        
        // Moons Score Title
        UILabel* moonsScoreTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [moonsScoreTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [moonsScoreTitle setTextColor:[UIColor whiteColor]];
        [moonsScoreTitle setTextAlignment:NSTextAlignmentCenter];
        [alertView addSubview:moonsScoreTitle];

        elementY += moonsScoreTitle.frame.size.height + verticalPadding;
        
        // Moons
        UIView* moonsView = [[UIView alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        UIImageView* firstMoon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"moon_dark"] highlightedImage:[UIImage imageNamed:@"moon_light"]];
        firstMoon.translatesAutoresizingMaskIntoConstraints = NO;
        UIImageView* secondMoon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"moon_dark"] highlightedImage:[UIImage imageNamed:@"moon_light"]];
        secondMoon.translatesAutoresizingMaskIntoConstraints = NO;
        UIImageView* thirdMoon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"moon_dark"] highlightedImage:[UIImage imageNamed:@"moon_light"]];
        thirdMoon.translatesAutoresizingMaskIntoConstraints = NO;
        [moonsView addSubview:firstMoon];
        [moonsView addSubview:secondMoon];
        [moonsView addSubview:thirdMoon];
        
        // constrain moons in moonsView
        // tie em together horizontally
        [moonsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[first]-40-[second]-40-[third]" options:0 metrics:nil views:@{@"first": firstMoon, @"second": secondMoon, @"third": thirdMoon}]];
        
        // center x
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:secondMoon attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        
        // center y
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:firstMoon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:secondMoon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:thirdMoon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        
        // height
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:firstMoon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:secondMoon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:thirdMoon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:moonsView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        
        // width
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:firstMoon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:firstMoon attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:secondMoon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:secondMoon attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        [moonsView addConstraint:[NSLayoutConstraint constraintWithItem:thirdMoon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:thirdMoon attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];
        
        [alertView addSubview:moonsView];
        
        // set highlight if necessary
        if (gameCode == kGameLost)
        {
            moonsView.hidden = YES;
        }
        else if (score.integerValue == 3)
        {
            [firstMoon setHighlighted:YES];
            [secondMoon setHighlighted:YES];
            [thirdMoon setHighlighted:YES];
            [moonsScoreTitle setText:@"Three Moons!!!"];
        }
        else if (score.integerValue == 2)
        {
            [firstMoon setHighlighted:YES];
            [secondMoon setHighlighted:YES];
            [moonsScoreTitle setText:@"Two Moons!!"];
        }
        else if (score.integerValue == 1)
        {
            [firstMoon setHighlighted:YES];
            [moonsScoreTitle setText:@"One Moon!"];
        }
        else
        {
            [moonsScoreTitle setText:@"Um.... no moons."];
        }
        
        elementY += moonsView.frame.size.height + verticalPadding;
        
        // current time title
        UILabel* currentTimeTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [currentTimeTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [currentTimeTitle setTextColor:[UIColor whiteColor]];
        [currentTimeTitle setTextAlignment:NSTextAlignmentCenter];
        [currentTimeTitle setText:@"Time"];
        [alertView addSubview:currentTimeTitle];
        
        elementY += currentTimeTitle.frame.size.height;
        
        // current time value
        UILabel* currentTimeValue = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [currentTimeValue setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightLight]];
        [currentTimeValue setTextColor:[UIColor whiteColor]];
        [currentTimeValue setTextAlignment:NSTextAlignmentCenter];
        NSString* timeString = [self timeFormatted:currentTime];
        if (gameCode == kGameQuit || gameCode == kGameLost)
        {
            timeString = @". . .";
        }
        [currentTimeValue setText:timeString];
        [alertView addSubview:currentTimeValue];
        
        elementY += currentTimeValue.frame.size.height + verticalPadding;
        
        // personal best title
        UILabel* personalBestTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [personalBestTitle setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightUltraLight]];
        [personalBestTitle setTextColor:[UIColor whiteColor]];
        [personalBestTitle setTextAlignment:NSTextAlignmentCenter];
        [personalBestTitle setText:@"Best Time"];
        [alertView addSubview:personalBestTitle];
        
        elementY += personalBestTitle.frame.size.height;
        
        // personal best value
        UILabel* personalBestValue = [[UILabel alloc] initWithFrame:CGRectMake(0, elementY, alertView.frame.size.width, 24)];
        [personalBestValue setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightLight]];
        [personalBestValue setTextColor:[UIColor whiteColor]];
        [personalBestValue setTextAlignment:NSTextAlignmentCenter];
        [personalBestValue setText:[self timeFormatted:@(bestTime)]];
        [alertView addSubview:personalBestValue];
        
        elementY += personalBestValue.frame.size.height + verticalPadding;
        
        // hide stuff if lost
        if (gameCode == kGameLost)
        {
            currentTimeTitle.hidden = YES;
            currentTimeValue.hidden = YES;
            personalBestTitle.hidden = YES;
            personalBestValue.hidden = YES;
        }
        
        // buttons are pinned to the bottom of the view
        // buttons
        int edgeMargin = 10;
        int horizontalPadding = 8;
        int buttonHeight = 60;
        int buttonX = edgeMargin;
        int buttonY = height - buttonHeight - edgeMargin;
        float middleButtonWidth = alertView.frame.size.width * 0.45;
        float sideButtonWidth;
        if (gameCode == kGameWon && moreLevels)
        {
            sideButtonWidth = ((alertView.frame.size.width - middleButtonWidth) - (edgeMargin * 2 + horizontalPadding * 2)) / 2;
        }
        else if (gameCode == kGameWon)
        {
            sideButtonWidth = (alertView.frame.size.width - (edgeMargin * 2));
        }
        else if (numberOfLives.integerValue > 0)
        {
            sideButtonWidth = (alertView.frame.size.width - (horizontalPadding + edgeMargin * 2)) / 2;
        }
        else
        {
            sideButtonWidth = (alertView.frame.size.width - (horizontalPadding + edgeMargin * 1));
        }
        
        // button 1 (left button)
        CGRect buttonFrame = CGRectMake(buttonX, buttonY, sideButtonWidth, buttonHeight);
        SPCYButton* button1 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"quit_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
        [button1 addTarget:self action:@selector(quitButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button1];
        
        buttonX += sideButtonWidth + horizontalPadding;
        
        // button 2 (middle button)
        buttonFrame = CGRectMake(buttonX, buttonY, middleButtonWidth, buttonHeight);
        SPCYButton* button2 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"play_overlay"] andBackgroundColor:[SPCYSettings spcyYellowColor]];
        [button2 addTarget:self action:@selector(playNextButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button2];
        if (gameCode == kGameWon && moreLevels)
        {
            [button2 setHidden:NO];
            buttonX += middleButtonWidth + horizontalPadding;
        }
        else
        {
            [button2 setHidden:YES];
        }
        
        // button 3 (right button)
        buttonFrame = CGRectMake(buttonX, buttonY, sideButtonWidth, buttonHeight);
        SPCYButton* button3 = [SPCYButton buttonWithFrame:buttonFrame overlayImage:[UIImage imageNamed:@"restart_overlay"] andBackgroundColor:[SPCYSettings spcyBlueColor]];
        [button3 addTarget:self action:@selector(replayButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [alertView addSubview:button3];
        
        if ((gameCode == kGameWon && !moreLevels) || numberOfLives == 0)
        {
            [button3 setHidden:YES];
        }
        
        [self addSubview:alertView];
        [self bringSubviewToFront:alertView];
    }
    
    return self;
}

- (void)quitButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeRight];
}

- (void)replayButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeLeft];
}

- (void)playNextButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeMiddle];
}

- (void)yesButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeOk];
}

- (void)noButtonPressed
{
    [self.alertDelegate alertView:self didSelectWithIndex:kTypeCancel];
}


#pragma mark - Utility

- (NSString*)timeFormatted:(NSNumber*)seconds
{
    NSNumberFormatter* nf = [[NSNumberFormatter alloc] init];
    [nf setFormatterBehavior:NSNumberFormatterBehaviorDefault];
    [nf setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString* numberAsString = [nf stringFromNumber:seconds];
    
    return numberAsString;
}

@end
