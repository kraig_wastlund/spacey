//
//  SPCYTextureLoader.h
//  spacey
//
//  Created by Chad Olsen on 5/16/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPCYTextureLoader : NSObject

+ (void)loadPlanetTextureToArray:(NSArray*)loadToArray;

@end
