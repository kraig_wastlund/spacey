//
//  SPCYCell.h
//  spacey
//
//  Created by Kraig Wastlund on 5/15/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPCYCell : UITableViewCell

/**
 * titleLabel
 @required
 * Left side property
 * Left label is set to form object's 'title' param
 * Each cell should have a left label value even if hidden.
 *
 * Optionally pass in a NSString as initial value in init.
 */
@property (nonatomic, strong) UILabel* titleLabel;

/**
 * subtitleLabel
 @optional
 * Left side property
 * Left label is set to form object's 'subtitle' param
 *
 * Optionally pass in a NSString as initial value in init.
 */
@property (nonatomic, strong) UILabel* subtitleLabel;

/**
 * rightComponent
 @optional
 * Right side property
 * This can be many different objects, check component class.
 *
 */
@property (nonatomic, strong) UIView* rightComponent;

/**
 * shouldReactToValidation
 @optional
 * Used as a flag to show/hide validation warnings on cell.
 *
 * If shouldReactToValidation is NO, all formatting
 * for showing warnings is ignored
 */
//@property (nonatomic) BOOL shouldReactToValidation;

/**
 * Use to calculate the cell's height
 * @return CGFloat
 */
+ (CGFloat)calculatedCellHeight;

@end
