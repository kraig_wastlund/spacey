//
//  SPCYFormObject.h
//  spacey
//
//  Created by Kraig Wastlund on 5/15/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SPCYConstants.h"

@interface SPCYFormObject : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* subTitle;
@property (nonatomic, strong) UIView* rightSideNormal;
@property (nonatomic, strong) UIView* rightSideSelected;
@property (nonatomic) BOOL selected;
@property (nonatomic, strong) NSString* productIdentifier; // used for market

@end
