//
//  UserLevel.h
//  spacey
//
//  Created by Donald Timpson on 5/14/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserLevel : NSManagedObject

+ (UserLevel*)getUserLevelWithLevelNumber:(NSNumber*)levelNumber InContext:(NSManagedObjectContext*)context;
- (void)setBestMoves:(NSNumber *)bestMoves;
- (void)setBestTime:(NSNumber *)bestTime;

@end

NS_ASSUME_NONNULL_END

#import "UserLevel+CoreDataProperties.h"
