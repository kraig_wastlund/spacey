//
//  SPCYCell.m
//  spacey
//
//  Created by Kraig Wastlund on 5/15/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "SPCYCell.h"
#import "SPCYSettings.h"

static const int horizontalPadding = 30;
static const int cellHeight = 62;


@interface SPCYCell ()

@property (nonatomic) BOOL didAddConstraints;
@property (nonatomic, strong) UIView* warningView;

@end


@implementation SPCYCell

#pragma mark - init

- (instancetype)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self buildHierarchy];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self buildHierarchy];
    }
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self buildHierarchy];
    }
    
    return self;
}


#pragma mark - Setters


#pragma mark - Getters

- (UILabel*)titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.textColor = [SPCYSettings spcyWhiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    return _titleLabel;
}

- (UILabel*)subtitleLabel
{
    if (!_subtitleLabel)
    {
        _subtitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _subtitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _subtitleLabel.textColor = [SPCYSettings spcyGrayColor];
        _subtitleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
        _subtitleLabel.textAlignment = NSTextAlignmentLeft;
        _subtitleLabel.numberOfLines = 0;
        _subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    
    return _subtitleLabel;
}

- (UIView*)rightComponent
{
    if (!_rightComponent)
    {
        _rightComponent = [[UIView alloc] initWithFrame:CGRectZero];
        _rightComponent.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _rightComponent;
}

//- (void)layoutSubviews // used by the textField didChange to hide/show warningView while textField text is being edited
//{
//    [super layoutSubviews];
//    [self updateConstraints];
//}


#pragma mark - Constraints

- (void)updateConstraints
{
    if (self.didAddConstraints)
    {
        [super updateConstraints];
        return;
    }
    
    NSDictionary* views = @{ @"titleLabel" : self.titleLabel, @"subtitleLabel" : self.subtitleLabel, @"rightComponent" : self.rightComponent };
    
    // horizontal
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%d-[titleLabel]-[rightComponent(80)]-%d-|", horizontalPadding, horizontalPadding] options:0 metrics:nil views:views]];
    
    // vertical
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-5-[titleLabel(20)][subtitleLabel]"] options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-5-[rightComponent]-5-|"] options:0 metrics:nil views:views]];
    
    // alignment
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.subtitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.titleLabel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.subtitleLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.titleLabel attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0]];
    
    self.didAddConstraints = YES;
    [super updateConstraints];
}


#pragma mark - Life Cycle

- (void)buildHierarchy
{
    [self setNeedsLayout];
    
    for (UIView* view in self.contentView.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.subtitleLabel];
    [self.contentView addSubview:self.rightComponent];
    [self updateConstraints];
}


#pragma mark - Cell Height

+ (CGFloat)calculatedCellHeight
{
    return cellHeight;
}

@end