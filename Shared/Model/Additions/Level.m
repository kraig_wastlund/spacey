//
//  Level+Additions.m
//  spacey
//
//  Created by Donald Timpson on 1/1/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

#import "Level.h"
#import "WormHole.h"
#import "BlackHole.h"

static const uint32_t playerCategory        =  0x1 << 0;
static const uint32_t middleCategory        =  0x1 << 1;
static const uint32_t wormHoleCategory      =  0x1 << 2;
static const uint32_t blackHoleCategory     =  0x1 << 3;
static const uint32_t endCategory           =  0x1 << 4;
static const double baseLevelTime = 2.5; // Initial time to look at map

@implementation Level

- (NSInteger)getNumberOfMoves
{
    return self.moves.integerValue * 2 + self.nodes.count / 4;
}

- (NSInteger)getTimeForPlayerSpeed:(NSInteger)playerSpeed andFPS:(CGFloat)fps
{
    NSInteger time;
    
    double speed = playerSpeed * fps;
    double distance = self.distance.doubleValue * self.cellSize.width;
    double distanceMod;
    
    if (self.cellSize.width < 60) {
        distanceMod = 5;
    } else if (self.cellSize.width < 70) {
        distanceMod = 4.5;
    } else if (self.cellSize.width < 80) {
        distanceMod = 4;
    } else if (self.cellSize.width < 90) {
        distanceMod = 3.8;
    } else if (self.cellSize.width < 100) {
        distanceMod = 3.7;
    } else if (self.cellSize.width < 110) {
        distanceMod = 3.6;
    } else {
        distanceMod = 3.5;
    }
    
    double timeForDistance = (distance/speed) * 5;
    double timeForNodes = ((double)self.nodes.count / 4);
    double timeForStoppingAtNodes = self.moves.integerValue * 0.45;
    
    time = baseLevelTime + timeForDistance + timeForStoppingAtNodes + timeForNodes + [self tutorialMod];
    
    return time;
}

- (Node*)start
{
    if (!_start)
    {
        _start = [[Node alloc] init];
    }
    
    return _start;
}

- (Node*)end
{
    if (!_end)
    {
        _end = [[Node alloc] init];
    }
    
    return _end;
}

- (NSArray*)middleNodes
{
    if (!_middleNodes)
    {
        _middleNodes = [[NSArray alloc] init];
    }
    
    return _middleNodes;
}

- (NSArray*)nodes
{
    if (!_nodes)
    {
        _nodes = [[NSArray alloc] init];
    }
    
    return _nodes;
}

- (NSInteger)tutorialMod
{
    NSArray* tutorialLevels = @[@0, @1, @2, @3, @4, @5, @21, @22, @23, @24, @25, @41, @42, @43, @44, @45];
    
    if ([tutorialLevels containsObject:self.levelNumber])
    {
        return 20 - (self.levelNumber.integerValue % 10) * 3;
    }
    
    return 0;
}

+ (Level*)getLevelWithName:(NSNumber*)levelNumber width:(NSNumber*)width height:(NSNumber*)height
{
    // grab the original plist from the GeneratedLevels dir
    NSString* levelName = [NSString stringWithFormat:@"Level%@", levelNumber];
    NSString* levelPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"plist"];
    NSDictionary* levelDictionary = [NSDictionary dictionaryWithContentsOfFile:levelPath];
    
    return [Level setupLevelWithDictionary:levelDictionary levelNumber:levelNumber width:width height:height];
}

+ (Level*)setupLevelWithDictionary:(NSDictionary*)levelDict levelNumber:(NSNumber*)levelNumber width:(NSNumber*)width height:(NSNumber*)height
{
    Level* newLevel = [[Level alloc] init];
    newLevel.width = width;
    newLevel.height = height;
    newLevel.levelNumber = levelNumber;
    newLevel.numberOfRows = [levelDict objectForKey:@"rows"];
    newLevel.numberOfColumns = [levelDict objectForKey:@"columns"];
    
    CGFloat gridSquareWidth = newLevel.width.doubleValue / newLevel.numberOfColumns.doubleValue;
    CGFloat gridSquareHeight = newLevel.height.doubleValue / newLevel.numberOfRows.doubleValue;
    newLevel.cellSize = CGSizeMake(gridSquareWidth, gridSquareHeight);
    
    NSDictionary* startNodeDict = [levelDict objectForKey:@"start"];
    NSDictionary* endNodeDict = [levelDict objectForKey:@"end"];
    NSArray* middleNodeDicts = [levelDict objectForKey:@"middle"];
    NSArray* blackHoleNodeDicts = [levelDict objectForKey:@"blackHoles"];
    NSArray* wormHoleNodeDicts = [levelDict objectForKey:@"wormHoles"];
    
    NSMutableArray* middleNodes = [[NSMutableArray alloc] init];
    NSMutableArray* wormHoleNodes = [[NSMutableArray alloc] init];
    NSMutableArray* blackHoleNodes = [[NSMutableArray alloc] init];
    
    for (NSDictionary* middleNodeDict in middleNodeDicts)
    {
        [middleNodes addObject:[Node setupNodeWithDictionary:middleNodeDict forLevel:newLevel]];
    }
    
    for (NSDictionary* blackHoleDict in blackHoleNodeDicts)
    {
        [blackHoleNodes addObject:[BlackHole setupBlackHoleWithDictionary:blackHoleDict forLevel:newLevel]];
    }
    
    for (NSDictionary* wormHoleNodeDict in wormHoleNodeDicts)
    {
        [wormHoleNodes addObject:[WormHole setupWormHoleWithDictionary:wormHoleNodeDict forLevel:newLevel]];
    }
    
    for (int i = 0; i < [wormHoleNodes count]; i++)
    {
        WormHole* wormHoleOne = wormHoleNodes[i];
        for (int j = 0; j < [wormHoleNodes count]; j++)
        {
            if (i != j && [wormHoleNodes[i] linkId].integerValue == [wormHoleNodes[j] linkId].integerValue)
            {
                WormHole* wormHoleTwo = wormHoleNodes[j];
                
                wormHoleOne.partnerNode = wormHoleTwo;
                wormHoleTwo.partnerNode = wormHoleOne;
                break;
            }
        }
    }
    
    newLevel.start = [Node setupNodeWithDictionary:startNodeDict forLevel:newLevel];
    newLevel.end = [Node setupNodeWithDictionary:endNodeDict forLevel:newLevel];
    newLevel.middleNodes = [NSArray arrayWithArray:middleNodes];
    newLevel.moves = [levelDict objectForKey:@"moves"];
    newLevel.distance = [levelDict objectForKey:@"distance"];
    newLevel.wormHoles = [NSArray arrayWithArray:wormHoleNodes];
    newLevel.blackHoles = [NSArray arrayWithArray:blackHoleNodes];
    newLevel.nodes = [newLevel.middleNodes arrayByAddingObjectsFromArray:newLevel.blackHoles];
    newLevel.nodes = [newLevel.nodes arrayByAddingObjectsFromArray:newLevel.wormHoles];
    newLevel.nodes = [newLevel.nodes arrayByAddingObject:newLevel.start];
    newLevel.nodes = [newLevel.nodes arrayByAddingObject:newLevel.end];
    
    newLevel.start.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:newLevel.start.size.width * 0.2];
    newLevel.start.physicsBody.dynamic = YES;
    newLevel.start.physicsBody.categoryBitMask = middleCategory;
    newLevel.start.physicsBody.contactTestBitMask = playerCategory;
    newLevel.start.physicsBody.collisionBitMask = 0;
    
    newLevel.end.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:newLevel.end.size.width * 0.2];
    newLevel.end.physicsBody.dynamic = YES;
    newLevel.end.physicsBody.categoryBitMask = endCategory;
    newLevel.end.physicsBody.contactTestBitMask = playerCategory;
    newLevel.end.physicsBody.collisionBitMask = 0;
    
    for (Node* node in newLevel.middleNodes)
    {
        node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:node.size.width * 0.2];
        node.physicsBody.dynamic = YES;
        node.physicsBody.categoryBitMask = middleCategory;
        node.physicsBody.contactTestBitMask = playerCategory;
        node.physicsBody.collisionBitMask = 0;
    }
    
    for (Node* node in newLevel.blackHoles)
    {
        node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:node.size.width * 0.2];
        node.physicsBody.dynamic = YES;
        node.physicsBody.categoryBitMask = blackHoleCategory;
        node.physicsBody.contactTestBitMask = playerCategory;
        node.physicsBody.collisionBitMask = 0;
    }
    
    for (Node* node in newLevel.wormHoles)
    {
        node.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:node.size.width * 0.2];
        node.physicsBody.dynamic = YES;
        node.physicsBody.categoryBitMask = wormHoleCategory;
        node.physicsBody.contactTestBitMask = playerCategory;
        node.physicsBody.collisionBitMask = 0;
    }
    
    return newLevel;
}

+ (NSNumber*)getScoreForLevel:(Level*)level withTime:(NSNumber*)time moves:(NSNumber*)moves fps:(NSNumber*)fps andPlayerSpeed:(NSNumber*)playerSpeed
{
    double timeScore = 1;
    double moveScore = 1;
    
    double timeAllowed = [level getTimeForPlayerSpeed:playerSpeed.integerValue andFPS:fps.doubleValue];
    

    if (time.doubleValue > (1.0 / 3.0) * timeAllowed)
    {
        timeScore = (timeAllowed - time.doubleValue) / timeAllowed;
        timeScore = timeScore * (3.0 / 2.0);
    }
    
    if (moves > level.moves)
    {
        moveScore = (2 * level.moves.doubleValue - moves.doubleValue) / level.moves.doubleValue;
        if (moveScore < 0)
        {
            moveScore = 0;
        }
    }
    double score = ((timeScore + moveScore) / 2);
    
    NSLog(@"score : %f", score);
    
    if (score == 1)
    {
        return @3;
    }
    else if (score > (2.0/3.0))
    {
        return @2;
    }
    else if (score > 1.0/3.0)
    {
        return @1;
    }
    
    return @0;
}

+ (NSInteger)getTotalNumberOfLevels
{
    NSString *bundleRoot = [[NSBundle mainBundle] bundlePath];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *dirContents = [fm contentsOfDirectoryAtPath:bundleRoot error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.plist' && self BEGINSWITH 'Level'"];
    NSArray *onlyPlists = [dirContents filteredArrayUsingPredicate:fltr];
    
    return [onlyPlists count];
}

@end