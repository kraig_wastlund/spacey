//
//  FlyingShip.h
//  spacey
//
//  Created by Kraig Wastlund on 11/27/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface FlyingShipNode : SKSpriteNode

+ (instancetype)flyingShipNodeWithFrame:(CGRect)frame;

@end
