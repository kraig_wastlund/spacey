//
//  MainMenu.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/13/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#ifdef TARGET_OS_IOS
    #import "spacey_iOS-Swift.h"
#elif TARGET_OS_TV
    #import "spacey_tvOS-Swift.h"
#endif

#import "MainScene.h"
#import "GameScene.h"
#import "LevelsScene.h"
#import "SoundController.h"
#import "FlyingShipNode.h"
#import "SPCYButton.h"
#import "SPCYSettings.h"
#import "SettingsTableViewController.h"
#import "MarketTableViewController.h"
@import AVFoundation;
#import "User.h"
#import "SPCYAlertView.h"
#import "SPCYConstants.h"


@interface MainScene () <SPCYAlertDelegate>

@property (nonatomic, strong) SKSpriteNode* background;
@property (nonatomic, strong) FlyingShipNode* ship;
@property (nonatomic) AVAudioPlayer* backgroundMusicPlayer;
@property (nonatomic, strong) SKLabelNode* timeTilNextRegenLabel;
@property (nonatomic, strong) User* user;
@property (nonatomic, strong) SPCYLivesNode* livesNode;

@end

@implementation MainScene

- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self)
    {
        SKSpriteNode *backgroundNode = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
        backgroundNode.size = size;
        backgroundNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        backgroundNode.name = @"BACKGROUND";
        [self addChild:backgroundNode];
        [backgroundNode setZPosition:-10];
    }
    
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    self.user = [User getUserInContext:self.managedObjectContext];
    
    [self.backgroundMusicPlayer play];
    
    view.scene.scaleMode = SKSceneScaleModeResizeFill;
    
    [self addNodes: view];
}

-(void)addNodes:(SKView *)view
{
    SKSpriteNode *backgroundNode = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
    backgroundNode.size = view.frame.size;
    backgroundNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundNode.name = @"BACKGROUND";
    [self addChild:backgroundNode];
    [backgroundNode setZPosition:-10];
    
    float titleWidth = self.frame.size.width;
    float titleHeight = titleWidth * 0.51;
    float elementY = view.frame.size.height - titleHeight + (0.10 * view.frame.size.height); // adjust last number to move title and planet up/down
    
    // spacey title
    SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
    title.size = CGSizeMake(titleWidth, titleHeight);
    title.position = CGPointMake(CGRectGetMidX(self.frame), elementY);
    title.zPosition = 1;
    [self addChild:title];
    
    elementY -= title.frame.size.height * 2;
    
    // rotating ship
    CGRect shipFrame = CGRectMake(0, elementY, view.frame.size.width, view.frame.size.width);
    self.ship = [FlyingShipNode flyingShipNodeWithFrame:shipFrame];
    [self addChild:self.ship];
    
    [self setupButtons];
}

-(void)willMoveFromView:(SKView *)view
{
    [self.ship removeAllActions];
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
}


#pragma mark - Buttons

- (void)updateLives
{
    [self.livesNode removeFromParent];
    self.livesNode = [SPCYLivesNode sharedLivesNodeInContext:self.managedObjectContext];
    [self.livesNode setPosition:CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 30)];
    [self addChild:self.livesNode];
    [self.livesNode reactToLivesChangedInContext:self.managedObjectContext];
}

- (void)setupButtons
{
    self.timeTilNextRegenLabel = [[SKLabelNode alloc] init];
    self.timeTilNextRegenLabel.fontName = @"HelveticaNeue-Light";
    self.timeTilNextRegenLabel.fontSize = 11;
    [self.timeTilNextRegenLabel setPosition:CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height - 54)];
    self.timeTilNextRegenLabel.alpha = 0.0;
    [self addChild:self.timeTilNextRegenLabel];
    
    float horizontalPaddingBetweenButtons = 5;
    float margin = 10;
    float buttonHeight = 60;
    float buttonY = self.view.frame.size.height - buttonHeight - margin;
    float middleButtonWidth = self.view.frame.size.width * 0.45;
    float sideButtonWidth = (self.view.frame.size.width - middleButtonWidth - (2 * margin) - (2 * horizontalPaddingBetweenButtons)) / 2;
    
    float smallButtonWidth = 50;
    float smallButtonHeight = 30;
    float topRightX = self.view.frame.size.width - margin - smallButtonWidth;
    SPCYButton* settingsButton = [SPCYButton buttonWithFrame:CGRectMake(topRightX, margin, smallButtonWidth, smallButtonHeight) overlayImage:[UIImage imageNamed:@"settings_overlay"] andBackgroundColor:[SPCYSettings spcyBlueColor]];
    settingsButton.alpha = 0.0;
    [settingsButton addTarget:self action:@selector(settingsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:settingsButton];
    
    float x = margin;
    SPCYButton* marketButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, sideButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"market_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
    marketButton.alpha = 0.0;
    [marketButton addTarget:self action:@selector(marketButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:marketButton];
    
    x += horizontalPaddingBetweenButtons + sideButtonWidth;
    
    SPCYButton* playButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, middleButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"play_overlay"] andBackgroundColor:[SPCYSettings spcyYellowColor]];
    playButton.alpha = 0.0;
    [playButton addTarget:self action:@selector(playButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
    
    x += horizontalPaddingBetweenButtons + middleButtonWidth;
    
    SPCYButton* levelsButton = [SPCYButton buttonWithFrame:CGRectMake(x, buttonY, sideButtonWidth, buttonHeight) overlayImage:[UIImage imageNamed:@"menu_overlay"] andBackgroundColor:[SPCYSettings spcyBlueColor]];
    levelsButton.alpha = 0.0;
    [levelsButton addTarget:self action:@selector(levelsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:levelsButton];
    
    [self performSelector:@selector(animateButtons:) withObject:@[settingsButton, playButton, levelsButton, marketButton] afterDelay:1.0];
}

- (void)animateButtons:(NSArray*)buttons
{
    SPCYButton* settingsButton = buttons[0];
    SPCYButton* playButton = buttons[1];
    SPCYButton* levelsButton = buttons[2];
    SPCYButton* marketButton = buttons[3];
    
    self.timeTilNextRegenLabel.text = [self.livesNode isFullWithContext:self.managedObjectContext] ? @"" : [self.user timeTilNextRegenerationMessage];
    
    [UIView animateWithDuration:0.5f animations:^{
        self.livesNode.alpha = 1.0;
        self.timeTilNextRegenLabel.alpha = [self.timeTilNextRegenLabel.text length] > 0 ? 1.0 : 0.0;
        settingsButton.alpha = 1.0;
        playButton.alpha = 1.0;
        levelsButton.alpha = 1.0;
        marketButton.alpha = 1.0;
    }];
}


#pragma mark - Actions

- (void)marketButtonPressed
{
    [self playClick];
    
    MarketTableViewController* vc = [[MarketTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
    vc.context = self.managedObjectContext;
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.view.window.rootViewController presentViewController:nc animated:YES completion:nil];
}

- (void)settingsButtonPressed
{
    [self playClick];
    
    SettingsTableViewController* vc = [[SettingsTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
    vc.context = self.managedObjectContext;
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.view.window.rootViewController presentViewController:nc animated:YES completion:nil];
}

- (void)playButtonPressed
{
    [self playClick];
    
    if (![self.livesNode isEmpty])
    {
        [self removeButtons];
        [self.backgroundMusicPlayer stop];
        
        GameScene* game = [[GameScene alloc] initWithSize:self.view.frame.size];
        game.managedObjectContext = self.managedObjectContext;
        game.scaleMode = SKSceneScaleModeResizeFill;
        [self.view presentScene:game transition:[SKTransition crossFadeWithDuration:0.5]];
    }
    else
    {
        SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithTitle:@"Out Of Lives" query:@"Are you sure?" andMessage:@"Would you like to see upgrades?"];
        alertView.alertDelegate = self;
        alertView.name = @"market";
        alertView.alpha = 0.0;
        alertView.tag = 99;
        [self.view addSubview:alertView];
        [UIView animateWithDuration:0.5
                         animations:^{alertView.alpha = 1.0;}
                         completion:^(BOOL finished){
                             // do stuff here
                         }];
    }
}

- (void)levelsButtonPressed
{
    [self playClick];
    
    [self removeButtons];
    
    LevelsScene* levels = [[LevelsScene alloc] initWithSize:self.view.frame.size];
    levels.scaleMode = SKSceneScaleModeResizeFill;
    levels.managedObjectContext = self.managedObjectContext;
    [self.view presentScene:levels transition:[SKTransition crossFadeWithDuration:0.5]];
}


#pragma mark - Timer

- (void)reactToTimer
{
    if ([self.livesNode isFullWithContext:self.managedObjectContext])
    {
        self.timeTilNextRegenLabel.text = @"";
    }
    else
    {
        self.timeTilNextRegenLabel.text = [self.user timeTilNextRegenerationMessage];
    }
}


#pragma mark - UIAnimations

- (void)removeButtons
{
    // fade buttons
    for (UIView* button in [self.view subviews])
    {
        [UIView animateWithDuration:0.5
                         animations:^{button.alpha = 0.0;}
                         completion:nil];
    }
    
    // remove buttons
    for (UIView* button in [self.view subviews])
    {
        [button removeFromSuperview];
    }
    
    [self removeAllChildren];
}


#pragma mark - AlertView Delegate

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType
{
    [self playClick];
    
    if (buttonType == kTypeOk)
    {
        if ([view.name isEqualToString:@"market"])
        {
            // do your thing
            [self marketButtonPressed];
        }
    }
    
    [UIView animateWithDuration:0.5
                     animations:^{view.alpha = 0.0;}
                     completion:^(BOOL finished){[view removeFromSuperview];}];
}


#pragma mark - Sounds

- (void)playClick
{
    if (self.user.soundEffectsOn.boolValue)
    {
        [self runAction:[SKAction playSoundFileNamed:soundStringClick waitForCompletion:NO]];
    }
}

@end
