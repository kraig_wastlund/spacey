//
//  KoldProducts.h
//  spacey
//
//  Created by Kraig Wastlund on 7/30/16.
//  Copyright © 2016 Kraig. All rights reserved.
//

@import Foundation;
#import "spacey_iOS-Swift.h"

NSString* spaceyIAP5Buckets();
NSString* spaceyIAP7Buckets();
NSString* spaceyIAP7BucketsAfter5();
NSString* spaceyIAP2xRegen();
NSString* spaceyIAP3xRegen();
NSString* spaceyIAP3xRegenAfter2();

@interface KoldProducts : NSObject

+ (IAPHelper*)store;
- (NSString*)resourceNameForProductIdentifier:(NSString*)productIdentifier;

@end
