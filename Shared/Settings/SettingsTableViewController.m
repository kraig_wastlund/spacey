//
//  SettingsTableViewController.m
//  code_camp_2015
//
//  Created by Chad Olsen on 11/14/15.
//  Copyright © 2015 Kraig. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "Player.h"
#import "Level.h"
#import "AboutViewController.h"
#import "SPCYSettings.h"
#import "SPCYAlertView.h"
#import "User.h"
#import "SPCYSettings.h"
#import "SPCYButton.h"
#import "SPCYCell.h"
#import "SPCYFormObject.h"
#import <spacey_iOS-Swift.h>

@interface SettingsTableViewController () <SPCYAlertDelegate>

@property (nonatomic, strong) NSArray* dataSource;
@property (nonatomic, strong) NSArray* sectionTitles;
@property (nonatomic, strong) User* currentUser;

@end

static const int sectionHeight = 46;
static const int horizontalPadding = 30;
static const int tableViewHeaderHeight = 50;

@implementation SettingsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentUser = [User getUserInContext:self.context];
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self setupDataSource];
    
    // set nav bar to be clear
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    // buttons
    // back button
    SPCYButton* backButton = [SPCYButton buttonWithFrame:CGRectMake(0, 0, 50, 30) overlayImage:[UIImage imageNamed:@"quit_overlay"] andBackgroundColor:[SPCYSettings spcyPinkColor]];
    [backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backButton]];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background"]]];
    
    [self.tableView registerClass:[SPCYCell class] forCellReuseIdentifier:@"cell"];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    [self setupTableViewHeader];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    // remove possible alerts
    for (UIView* view in self.view.subviews)
    {
        if (view.tag == 99)
        {
            [view removeFromSuperview];
        }
    }
    
    [self.tableView reloadData];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}


#pragma mark - TableView

- (void)setupTableViewHeader
{
    UIView* headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, tableViewHeaderHeight)];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPadding, 0, self.tableView.frame.size.width - (horizontalPadding * 2), tableViewHeaderHeight - 1)];
    titleLabel.text = @"Settings";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:32 weight:UIFontWeightThin];
    [headerView addSubview:titleLabel];
    
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(horizontalPadding, titleLabel.frame.size.height, self.tableView.frame.size.width - (horizontalPadding * 2), 1.0)];
    [line setBackgroundColor:[SPCYSettings spcyBlueColor]];
    [headerView addSubview:line];
    
    self.tableView.tableHeaderView = headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sectionHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.dataSource objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [SPCYCell calculatedCellHeight];
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    int titleHeight = 20;
    
    NSString* sectionTitle = [self.sectionTitles objectAtIndex:section];
    
    UIView* sectionView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.tableView.frame.size.width, sectionHeight)];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(horizontalPadding, sectionHeight - titleHeight, sectionView.frame.size.width - (horizontalPadding * 2), titleHeight)];
    [titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightUltraLight]];
    [titleLabel setTextColor:[SPCYSettings spcyBlueColor]];
    titleLabel.text = sectionTitle;
    
    [sectionView addSubview:titleLabel];
    
    return sectionView;
}

- (SPCYCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    SPCYCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[SPCYCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(SPCYCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SPCYFormObject* formObject = [[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = formObject.title;
    cell.subtitleLabel.text = formObject.subTitle;
    if (formObject.rightSideNormal)
    {
        [cell.rightComponent addSubview: formObject.rightSideNormal];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rightSideNormal]|" options:0 metrics:nil views:@{ @"rightSideNormal": formObject.rightSideNormal }]];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightSideNormal]|" options:0 metrics:nil views:@{ @"rightSideNormal": formObject.rightSideNormal }]];
        
        [cell.rightComponent addSubview: formObject.rightSideSelected];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[rightSideSelected]|" options:0 metrics:nil views:@{ @"rightSideSelected": formObject.rightSideSelected }]];
        [cell.rightComponent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightSideSelected]|" options:0 metrics:nil views:@{ @"rightSideSelected": formObject.rightSideSelected }]];
        
        if (formObject.selected)
        {
            formObject.rightSideNormal.alpha = 0.0;
            formObject.rightSideSelected.alpha = 1.0;
        }
        else
        {
            formObject.rightSideNormal.alpha = 1.0;
            formObject.rightSideSelected.alpha = 0.0;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPCYFormObject* formObject = [[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    formObject.selected = !formObject.selected;
    
    if ([formObject.title isEqualToString:@"Sound Effects"])
    {
        [self soundToggleToOn:formObject.selected];
    }
    else if ([formObject.title isEqualToString:@"Background Music"])
    {
        [self musicToggleToOn:formObject.selected];
    }
    else if ([formObject.title isEqualToString:@"Vibrate"])
    {
        [self vibrateToggleToOn:formObject.selected];
    }
    else if ([formObject.title isEqualToString:@"Credits"])
    {
        AboutViewController* avc = [[AboutViewController alloc] init];
        [self.navigationController pushViewController:avc animated:YES];
    }
    else if ([formObject.title isEqualToString:@"Reset?"])
    {
        [self triggerAlertWithFormObject:formObject];
    }
    else if ([formObject.title isEqualToString:@"Unlock?"])
    {
        [self triggerAlertWithFormObject:formObject];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self animateSelectionOfApplicableFormObjects];
}

- (void)animateSelectionOfApplicableFormObjects
{
    NSMutableArray* animatingObjects = [[NSMutableArray alloc] init];
    
    for (int section = 0; section < [self.dataSource count]; section++)
    {
        for (SPCYFormObject* object in [self.dataSource objectAtIndex:section])
        {
            if (object.selected != (object.rightSideSelected.alpha == 1.0))
            {
                [animatingObjects addObject:object];
            }
        }
    }
    
    if ([animatingObjects count] > 0)
    {
        [UIView animateWithDuration:0.25 animations:^{
            
            for (SPCYFormObject* formObject in animatingObjects)
            {
                if (formObject.selected)
                {
                    formObject.rightSideNormal.alpha = 0.0;
                }
                else
                {
                    formObject.rightSideSelected.alpha = 0.0;
                }
            }
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.25 animations:^{
                
                for (SPCYFormObject* formObject in animatingObjects)
                {
                    if (formObject.selected)
                    {
                        formObject.rightSideSelected.alpha = 1.0;
                    }
                    else
                    {
                        formObject.rightSideNormal.alpha = 1.0;
                    }
                }
            }];
        }];
    }
}


#pragma mark - Data Source

- (UIView*)selectedView
{
    UIView* selectedView = [[UIView alloc] initWithFrame:CGRectZero];
    
    selectedView.translatesAutoresizingMaskIntoConstraints = NO;
    UILabel* selectedLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    selectedLabel.translatesAutoresizingMaskIntoConstraints = NO;
    selectedLabel.text = @"ON";
    selectedLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightLight];
    selectedLabel.textColor = [SPCYSettings spcyGreenColor];
    selectedLabel.textAlignment = NSTextAlignmentCenter;
    [selectedView addSubview:selectedLabel];
    [selectedView addConstraint:[NSLayoutConstraint constraintWithItem:selectedLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:selectedView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [selectedView addConstraint:[NSLayoutConstraint constraintWithItem:selectedLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:selectedView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [selectedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(38)]" options:0 metrics:nil views:@{@"view": selectedLabel}]];

    return selectedView;
}

- (UIView*)unSelectedView
{
    UIView* unSelectedView = [[UIView alloc] initWithFrame:CGRectZero];
    
    unSelectedView.translatesAutoresizingMaskIntoConstraints = NO;
    UILabel* unSelectedLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    unSelectedLabel.translatesAutoresizingMaskIntoConstraints = NO;
    unSelectedLabel.text = @"OFF";
    unSelectedLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightLight];
    unSelectedLabel.textColor = [SPCYSettings spcyGreenColor];
    unSelectedLabel.textAlignment = NSTextAlignmentCenter;
    [unSelectedView addSubview:unSelectedLabel];
    [unSelectedView addConstraint:[NSLayoutConstraint constraintWithItem:unSelectedLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:unSelectedView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0]];
    [unSelectedView addConstraint:[NSLayoutConstraint constraintWithItem:unSelectedLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:unSelectedView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [unSelectedView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[view(38)]" options:0 metrics:nil views:@{@"view": unSelectedLabel}]];
    
    return unSelectedView;
}

- (void)setupDataSource
{
    SPCYFormObject* soundObject = [[SPCYFormObject alloc] init];
    soundObject.selected = self.currentUser.soundEffectsOn.boolValue;
    soundObject.title = @"Sound Effects";
    soundObject.subTitle = @"You know, the bleeps tweeps, and buzzles.";
    soundObject.rightSideNormal = [self unSelectedView];
    soundObject.rightSideSelected = [self selectedView];
    
    SPCYFormObject* musicObject = [[SPCYFormObject alloc] init];
    musicObject.selected = self.currentUser.musicOn.boolValue;
    musicObject.title = @"Background Music";
    musicObject.subTitle = @"Do you need theme music?";
    musicObject.rightSideNormal = [self unSelectedView];
    musicObject.rightSideSelected = [self selectedView];
    
    SPCYFormObject* vibrateObject = [[SPCYFormObject alloc] init];
    vibrateObject.selected = self.currentUser.vibrateOn.boolValue;
    vibrateObject.title = @"Vibrate";
    vibrateObject.subTitle = @"Careful, your device might vibrate right out of your hand!";
    vibrateObject.rightSideNormal = [self unSelectedView];
    vibrateObject.rightSideSelected = [self selectedView];
    
    SPCYFormObject* creditsObject = [[SPCYFormObject alloc] init];
    creditsObject.title = @"Credits";
    creditsObject.subTitle = @"To whom do we owe the pleasure?";
    
    SPCYFormObject* resetObject = [[SPCYFormObject alloc] init];
    resetObject.title = @"Reset?";
    resetObject.subTitle = @"Would you like to reset back to zero?";
    
    SPCYFormObject* unlockObject = [[SPCYFormObject alloc] init];
    unlockObject.title = @"Unlock?";
    unlockObject.subTitle = @"Unlock all levels?";
    
    if ([SPCYSettings spcyDebugToggle]) {
        self.dataSource = @[
                            @[soundObject, musicObject, vibrateObject],
                            @[creditsObject],
                            @[resetObject, unlockObject]
                            ];
    } else {
        self.dataSource = @[
                            @[soundObject, musicObject, vibrateObject],
                            @[creditsObject]
                            ];
    }
    
    self.sectionTitles = @[@"EFFECTS", @"DOCS", @"OP"];
}


#pragma mark - Alert

- (void)triggerAlertWithFormObject:(SPCYFormObject*)formObject
{
    NSString* title = [self alertTitleForFormObject:formObject];
    NSString* message = [self alertMessageForFormObject:formObject];
    
    SPCYAlertView* alertView = [[SPCYAlertView alloc] initWithTitle:title query:@"Are you sure?" andMessage:message];
    alertView.alertDelegate = self;
    alertView.name = formObject.title;
    alertView.alpha = 0.0;
    alertView.tag = 99;
    [self.parentViewController.view addSubview:alertView];
    [UIView animateWithDuration:0.5
                     animations:^{alertView.alpha = 1.0;}
                     completion:^(BOOL finished){
                     }];
}

- (NSString*)alertTitleForFormObject:(SPCYFormObject*)formObject
{
    return formObject.title;
}

- (NSString*)alertMessageForFormObject:(SPCYFormObject*)formObject
{
    NSString* retString;
    
    if ([formObject.title isEqualToString:@"Reset?"])
    {
        retString = @"Are you certain you'd like to reset all the levels?";
    }
    else if ([formObject.title isEqualToString:@"Unlock?"])
    {
        retString = @"Do you know what you're about to do?  The ramifications could be deadly.";
    }
    else
    {
        assert(YES == NO); // you should never get here!
    }
    
    return retString;
}


#pragma mark - actions

- (void)backButtonPressed
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)musicToggleToOn:(BOOL)isOn
{
    [self.currentUser setMusicOn:@(isOn)];
    GameViewController* vc = (GameViewController*)self.navigationController.presentingViewController;
    [vc handleMusic];
}

- (void)vibrateToggleToOn:(BOOL)isOn
{
    [self.currentUser setVibrateOn:@(isOn)];
}

- (void)soundToggleToOn:(BOOL)isOn
{
    [self.currentUser setSoundEffectsOn:@(isOn)];
}


#pragma mark - AlertView Delegate

- (void)alertView:(SPCYAlertView*)view didSelectWithIndex:(AlertButtonType)buttonType
{
    if (buttonType == kTypeOk)
    {
        if ([view.name isEqualToString:@"Reset?"])
        {
            self.currentUser.lastPlayedLevel = @1;
            self.currentUser.highestCompletedLevel = @0;
        }
        else if ([view.name isEqualToString:@"Unlock?"])
        {
            self.currentUser.highestCompletedLevel = [NSNumber numberWithInteger:[Level getTotalNumberOfLevels]];
            self.currentUser.lastPlayedLevel = [NSNumber numberWithInteger:[Level getTotalNumberOfLevels]];
        }
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 0.0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

@end
